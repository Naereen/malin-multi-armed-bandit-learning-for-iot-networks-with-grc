#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: IoT Gateway
# Author: Remi Bonnefoi and Lilian Besson
# Description: Gateway listening to our objects and replying with acknowledgments
# Generated: Thu Jul 26 16:13:42 2018
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from optparse import OptionParser
import Check_ack
import Renormalize_ack
import Send_ack
import sip
import sys
import time
from gnuradio import qtgui


class USRP_RX_BTS__v1(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "IoT Gateway")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("IoT Gateway")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "USRP_RX_BTS__v1")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Variables
        ##################################################
        self.speed_up_factor = speed_up_factor = 2
        self.seconds_packet = seconds_packet = 1.0 / float(speed_up_factor)
        self.samp_rate = samp_rate = int(32000*speed_up_factor)
        self.Packet_l = Packet_l = int(samp_rate * seconds_packet)
        self.Nb_ch = Nb_ch = 16
        self.digital_gain_default = digital_gain_default = 0.01
        self.Preambl_l = Preambl_l = int(Packet_l / 32)
        self.Delay_before_ack = Delay_before_ack = int(Packet_l * 1)
        self.Active_ch = Active_ch = list(range(Nb_ch))
        self.variable_qtgui_label_2 = variable_qtgui_label_2 = "Packet of {} second{}".format(seconds_packet*speed_up_factor, "s" if (seconds_packet*speed_up_factor) > 1 else "") + ", {:.3g} second{} before ack".format(Delay_before_ack/float(samp_rate), "s" if Delay_before_ack/float(samp_rate) > 1 else "")
        self.variable_qtgui_label_1 = variable_qtgui_label_1 = ", ".join(["#{:d}".format(ch) for ch in Active_ch])
        self.variable_qtgui_label_0 = variable_qtgui_label_0 = ''
        self.digital_gain = digital_gain = digital_gain_default
        self.center_freq = center_freq = 433.5e6
        self.analog_gain = analog_gain = 1
        self.address = address = "addr=192.168.10.105"
        self.FFT_Size = FFT_Size = Nb_ch
        self.Demod_threshold = Demod_threshold = 0.01
        self.Ack_length = Ack_length = Packet_l + Preambl_l

        ##################################################
        # Blocks
        ##################################################
        self._variable_qtgui_label_2_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_2_formatter = None
        else:
          self._variable_qtgui_label_2_formatter = lambda x: x

        self._variable_qtgui_label_2_tool_bar.addWidget(Qt.QLabel('Speed'+": "))
        self._variable_qtgui_label_2_label = Qt.QLabel(str(self._variable_qtgui_label_2_formatter(self.variable_qtgui_label_2)))
        self._variable_qtgui_label_2_tool_bar.addWidget(self._variable_qtgui_label_2_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_2_tool_bar, 2,0,1,1)

        self._variable_qtgui_label_1_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_1_formatter = None
        else:
          self._variable_qtgui_label_1_formatter = lambda x: x

        self._variable_qtgui_label_1_tool_bar.addWidget(Qt.QLabel('Active channels'+": "))
        self._variable_qtgui_label_1_label = Qt.QLabel(str(self._variable_qtgui_label_1_formatter(self.variable_qtgui_label_1)))
        self._variable_qtgui_label_1_tool_bar.addWidget(self._variable_qtgui_label_1_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_1_tool_bar, 1,0,1,1)

        self._variable_qtgui_label_0_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_0_formatter = None
        else:
          self._variable_qtgui_label_0_formatter = lambda x: x

        self._variable_qtgui_label_0_tool_bar.addWidget(Qt.QLabel('<h2>IoT Gateway</h2> listen to our objects and reply with Acks<br>Demo for ICT 2018<br>By Lilian Besson and Remi Bonnefoi'+": "))
        self._variable_qtgui_label_0_label = Qt.QLabel(str(self._variable_qtgui_label_0_formatter(self.variable_qtgui_label_0)))
        self._variable_qtgui_label_0_tool_bar.addWidget(self._variable_qtgui_label_0_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_0_tool_bar, 0,0,1,3)

        self.uhd_usrp_source_0 = uhd.usrp_source(
        	",".join((address, "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source_0.set_clock_source('external', 0)
        self.uhd_usrp_source_0.set_time_source('external', 0)
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0.set_time_unknown_pps(uhd.time_spec())
        self.uhd_usrp_source_0.set_center_freq(uhd.tune_request(center_freq,1.2e6), 0)
        self.uhd_usrp_source_0.set_gain(0, 0)
        self.uhd_usrp_source_0.set_antenna('RX2', 0)
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
        	",".join((address, "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0.set_clock_source('external', 0)
        self.uhd_usrp_sink_0.set_time_source('external', 0)
        self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0.set_time_now(uhd.time_spec(time.time()), uhd.ALL_MBOARDS)
        self.uhd_usrp_sink_0.set_center_freq(uhd.tune_request(center_freq,1.2e6), 0)
        self.uhd_usrp_sink_0.set_gain(analog_gain, 0)
        self.uhd_usrp_sink_0.set_antenna('TX/RX', 0)
        self.qtgui_sink_x_1_1 = qtgui.sink_c(
        	FFT_Size, #fftsize
        	firdes.WIN_RECTANGULAR, #wintype
        	center_freq, #fc
        	samp_rate, #bw
        	"USRP_RX_BTS Source", #name
        	False, #plotfreq
        	True, #plotwaterfall
        	False, #plottime
        	False, #plotconst
        )
        self.qtgui_sink_x_1_1.set_update_time(1.0/10)
        self._qtgui_sink_x_1_1_win = sip.wrapinstance(self.qtgui_sink_x_1_1.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_sink_x_1_1_win, 3,0,1,1)

        self.qtgui_sink_x_1_1.enable_rf_freq(True)



        self.qtgui_sink_x_1_0_0_0 = qtgui.sink_c(
        	Nb_ch, #fftsize
        	firdes.WIN_RECTANGULAR, #wintype
        	center_freq, #fc
        	samp_rate, #bw
        	"USRP_RX_BTS Sink", #name
        	False, #plotfreq
        	True, #plotwaterfall
        	False, #plottime
        	False, #plotconst
        )
        self.qtgui_sink_x_1_0_0_0.set_update_time(1.0/10)
        self._qtgui_sink_x_1_0_0_0_win = sip.wrapinstance(self.qtgui_sink_x_1_0_0_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_sink_x_1_0_0_0_win, 4,0,1,1)

        self.qtgui_sink_x_1_0_0_0.enable_rf_freq(True)



        self.fft_vxx_0_0 = fft.fft_vcc(FFT_Size, False, (window.rectangular(FFT_Size)), True, 1)
        self.fft_vxx_0 = fft.fft_vcc(FFT_Size, True, (window.rectangular(FFT_Size)), True, 1)
        self.blocks_vector_to_stream_0_1_0_0_0_1_0 = blocks.vector_to_stream(gr.sizeof_gr_complex*1, Nb_ch)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, FFT_Size)
        self.blocks_multiply_const_vxx_0_1 = blocks.multiply_const_vcc((digital_gain**2, ))
        self.blocks_multiply_const_vxx_0_0_1 = blocks.multiply_const_vcc((0.035, ))
        self.blocks_multiply_const_vxx_0_0 = blocks.multiply_const_vcc((1.0/digital_gain if (digital_gain > 0) else 1.0, ))
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vcc((digital_gain, ))
        self.Send_ack_send_ack_v2_ic_0 = Send_ack.Send_ack_v2(Nb_ch, (Active_ch), Delay_before_ack, Ack_length, Preambl_l)
        self.Renormalize_ack_Renormalize_ack_v3_0 = Renormalize_ack.Renormalize_ack_v3(Demod_threshold, Nb_ch, (Active_ch), int(Preambl_l/10))
        self.Check_ack_Check_ack_V2_0 = Check_ack.Check_ack_V2(Nb_ch, (Active_ch), Packet_l, 0.05, 0.01, True, 0, 0)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.Check_ack_Check_ack_V2_0, 0), (self.Send_ack_send_ack_v2_ic_0, 0))
        self.connect((self.Renormalize_ack_Renormalize_ack_v3_0, 0), (self.Check_ack_Check_ack_V2_0, 0))
        self.connect((self.Send_ack_send_ack_v2_ic_0, 0), (self.fft_vxx_0_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_multiply_const_vxx_0_0_1, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.uhd_usrp_sink_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.blocks_multiply_const_vxx_0_1, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0_1, 0), (self.qtgui_sink_x_1_0_0_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_1, 0), (self.qtgui_sink_x_1_1, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.fft_vxx_0, 0))
        self.connect((self.blocks_vector_to_stream_0_1_0_0_0_1_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.fft_vxx_0, 0), (self.Renormalize_ack_Renormalize_ack_v3_0, 0))
        self.connect((self.fft_vxx_0_0, 0), (self.blocks_vector_to_stream_0_1_0_0_0_1_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.blocks_multiply_const_vxx_0_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "USRP_RX_BTS__v1")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_speed_up_factor(self):
        return self.speed_up_factor

    def set_speed_up_factor(self, speed_up_factor):
        self.speed_up_factor = speed_up_factor
        self.set_samp_rate(int(32000*self.speed_up_factor))
        self.set_variable_qtgui_label_2(self._variable_qtgui_label_2_formatter("Packet of {} second{}".format(self.seconds_packet*self.speed_up_factor, "s" if (self.seconds_packet*self.speed_up_factor) > 1 else "") + ", {:.3g} second{} before ack".format(self.Delay_before_ack/float(self.samp_rate), "s" if self.Delay_before_ack/float(self.samp_rate) > 1 else "")))
        self.set_seconds_packet(1.0 / float(self.speed_up_factor))

    def get_seconds_packet(self):
        return self.seconds_packet

    def set_seconds_packet(self, seconds_packet):
        self.seconds_packet = seconds_packet
        self.set_Packet_l(int(self.samp_rate * self.seconds_packet))
        self.set_variable_qtgui_label_2(self._variable_qtgui_label_2_formatter("Packet of {} second{}".format(self.seconds_packet*self.speed_up_factor, "s" if (self.seconds_packet*self.speed_up_factor) > 1 else "") + ", {:.3g} second{} before ack".format(self.Delay_before_ack/float(self.samp_rate), "s" if self.Delay_before_ack/float(self.samp_rate) > 1 else "")))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_Packet_l(int(self.samp_rate * self.seconds_packet))
        self.set_variable_qtgui_label_2(self._variable_qtgui_label_2_formatter("Packet of {} second{}".format(self.seconds_packet*self.speed_up_factor, "s" if (self.seconds_packet*self.speed_up_factor) > 1 else "") + ", {:.3g} second{} before ack".format(self.Delay_before_ack/float(self.samp_rate), "s" if self.Delay_before_ack/float(self.samp_rate) > 1 else "")))
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)
        self.qtgui_sink_x_1_1.set_frequency_range(self.center_freq, self.samp_rate)
        self.qtgui_sink_x_1_0_0_0.set_frequency_range(self.center_freq, self.samp_rate)

    def get_Packet_l(self):
        return self.Packet_l

    def set_Packet_l(self, Packet_l):
        self.Packet_l = Packet_l
        self.set_Preambl_l(int(self.Packet_l / 32))
        self.set_Delay_before_ack(int(self.Packet_l * 1))
        self.set_Ack_length(self.Packet_l + self.Preambl_l)

    def get_Nb_ch(self):
        return self.Nb_ch

    def set_Nb_ch(self, Nb_ch):
        self.Nb_ch = Nb_ch
        self.set_FFT_Size(self.Nb_ch)
        self.set_Active_ch(list(range(self.Nb_ch)))

    def get_digital_gain_default(self):
        return self.digital_gain_default

    def set_digital_gain_default(self, digital_gain_default):
        self.digital_gain_default = digital_gain_default
        self.set_digital_gain(self.digital_gain_default)

    def get_Preambl_l(self):
        return self.Preambl_l

    def set_Preambl_l(self, Preambl_l):
        self.Preambl_l = Preambl_l
        self.set_Ack_length(self.Packet_l + self.Preambl_l)

    def get_Delay_before_ack(self):
        return self.Delay_before_ack

    def set_Delay_before_ack(self, Delay_before_ack):
        self.Delay_before_ack = Delay_before_ack
        self.set_variable_qtgui_label_2(self._variable_qtgui_label_2_formatter("Packet of {} second{}".format(self.seconds_packet*self.speed_up_factor, "s" if (self.seconds_packet*self.speed_up_factor) > 1 else "") + ", {:.3g} second{} before ack".format(self.Delay_before_ack/float(self.samp_rate), "s" if self.Delay_before_ack/float(self.samp_rate) > 1 else "")))

    def get_Active_ch(self):
        return self.Active_ch

    def set_Active_ch(self, Active_ch):
        self.Active_ch = Active_ch
        self.set_variable_qtgui_label_1(self._variable_qtgui_label_1_formatter(", ".join(["#{:d}".format(ch) for ch in self.Active_ch])))

    def get_variable_qtgui_label_2(self):
        return self.variable_qtgui_label_2

    def set_variable_qtgui_label_2(self, variable_qtgui_label_2):
        self.variable_qtgui_label_2 = variable_qtgui_label_2
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_2_label, "setText", Qt.Q_ARG("QString", str(self.variable_qtgui_label_2)))

    def get_variable_qtgui_label_1(self):
        return self.variable_qtgui_label_1

    def set_variable_qtgui_label_1(self, variable_qtgui_label_1):
        self.variable_qtgui_label_1 = variable_qtgui_label_1
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_1_label, "setText", Qt.Q_ARG("QString", repr(self.variable_qtgui_label_1)))

    def get_variable_qtgui_label_0(self):
        return self.variable_qtgui_label_0

    def set_variable_qtgui_label_0(self, variable_qtgui_label_0):
        self.variable_qtgui_label_0 = variable_qtgui_label_0
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_0_label, "setText", Qt.Q_ARG("QString", str(self.variable_qtgui_label_0)))

    def get_digital_gain(self):
        return self.digital_gain

    def set_digital_gain(self, digital_gain):
        self.digital_gain = digital_gain
        self.blocks_multiply_const_vxx_0_1.set_k((self.digital_gain**2, ))
        self.blocks_multiply_const_vxx_0_0.set_k((1.0/self.digital_gain if (self.digital_gain > 0) else 1.0, ))
        self.blocks_multiply_const_vxx_0.set_k((self.digital_gain, ))

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.uhd_usrp_source_0.set_center_freq(uhd.tune_request(self.center_freq,1.2e6), 0)
        self.uhd_usrp_sink_0.set_center_freq(uhd.tune_request(self.center_freq,1.2e6), 0)
        self.qtgui_sink_x_1_1.set_frequency_range(self.center_freq, self.samp_rate)
        self.qtgui_sink_x_1_0_0_0.set_frequency_range(self.center_freq, self.samp_rate)

    def get_analog_gain(self):
        return self.analog_gain

    def set_analog_gain(self, analog_gain):
        self.analog_gain = analog_gain
        self.uhd_usrp_sink_0.set_gain(self.analog_gain, 0)


    def get_address(self):
        return self.address

    def set_address(self, address):
        self.address = address

    def get_FFT_Size(self):
        return self.FFT_Size

    def set_FFT_Size(self, FFT_Size):
        self.FFT_Size = FFT_Size

    def get_Demod_threshold(self):
        return self.Demod_threshold

    def set_Demod_threshold(self, Demod_threshold):
        self.Demod_threshold = Demod_threshold

    def get_Ack_length(self):
        return self.Ack_length

    def set_Ack_length(self, Ack_length):
        self.Ack_length = Ack_length


def main(top_block_cls=USRP_RX_BTS__v1, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
