#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: IoT Object Number 3
# Author: Remi Bonnefoi and Lilian Besson
# Description: Intelligent dynamic user using UCB algorithm to learn the best channel
# Generated: Thu Jul 26 16:21:09 2018
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from optparse import OptionParser
import Check_ack
import Renormalize_ack
import generator_SU
import sip
import sys
import time
from gnuradio import qtgui


class USRP_TX_SU__v1__obj3(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "IoT Object Number 3")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("IoT Object Number 3")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "USRP_TX_SU__v1__obj3")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Variables
        ##################################################
        self.speed_up_factor = speed_up_factor = 2
        self.seconds_packet = seconds_packet = 1.0 / float(speed_up_factor)
        self.samp_rate = samp_rate = int(32000*speed_up_factor)
        self.idnumber = idnumber = 3
        self.use_uniform_selection = use_uniform_selection = idnumber == 2
        self.use_thompson = use_thompson = idnumber == 3
        self.short_message_to_send = short_message_to_send = ""
        self.seconds_between_packets = seconds_between_packets = 2.5
        self.real_part_data = real_part_data = +1 if idnumber == 2 else -1
        self.imag_part_data = imag_part_data = +1
        self.digital_gain_default = digital_gain_default = 0.01
        self.Packet_l = Packet_l = int(samp_rate * seconds_packet)
        self.Nb_ch = Nb_ch = 16
        self.Active_ch = Active_ch = [1,5,9,12] if idnumber==1 else ([2,6,10,13] if idnumber==2 else [3,7,11,14])
        self.variable_qtgui_label_3 = variable_qtgui_label_3 = "Packet of {} second{}, every {} second{}".format(seconds_packet*speed_up_factor, "s" if (seconds_packet*speed_up_factor) > 1 else "", seconds_between_packets, "s" if seconds_between_packets > 1 else "")
        self.variable_qtgui_label_2 = variable_qtgui_label_2 = ("Uniform access" if use_uniform_selection else ("Thompson Sampling" if use_thompson else "UCB")) + ("" if short_message_to_send == "" else ", sending '{}'".format(short_message_to_send)) + " and symbols " + str(real_part_data) + " +- " + str(imag_part_data) + "j"
        self.variable_qtgui_label_1 = variable_qtgui_label_1 = ", ".join(["#{:d}".format(ch) for ch in Active_ch])
        self.variable_qtgui_label_0 = variable_qtgui_label_0 = 'Demo for ICT 2018<br>By Lilian Besson and Remi Bonnefoi'
        self.digital_gain = digital_gain = digital_gain_default
        self.center_freq = center_freq = 433.5e6
        self.analog_gain = analog_gain = 1
        self.address = address = "addr=192.168.10.104" if idnumber==1 else ("addr=192.168.10.109" if idnumber==2 else "addr=192.168.10.114")
        self.Preambl_l = Preambl_l = int(Packet_l / 32)
        self.FFT_Size = FFT_Size = Nb_ch
        self.Demod_threshold = Demod_threshold = 0.01

        ##################################################
        # Blocks
        ##################################################
        _digital_gain_check_box = Qt.QCheckBox('Enable transmission')
        self._digital_gain_choices = {True: digital_gain_default, False: 0}
        self._digital_gain_choices_inv = dict((v,k) for k,v in self._digital_gain_choices.iteritems())
        self._digital_gain_callback = lambda i: Qt.QMetaObject.invokeMethod(_digital_gain_check_box, "setChecked", Qt.Q_ARG("bool", self._digital_gain_choices_inv[i]))
        self._digital_gain_callback(self.digital_gain)
        _digital_gain_check_box.stateChanged.connect(lambda i: self.set_digital_gain(self._digital_gain_choices[bool(i)]))
        self.top_grid_layout.addWidget(_digital_gain_check_box, 2,0,1,1)
        self._variable_qtgui_label_3_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_3_formatter = None
        else:
          self._variable_qtgui_label_3_formatter = lambda x: x

        self._variable_qtgui_label_3_tool_bar.addWidget(Qt.QLabel('Speed'+": "))
        self._variable_qtgui_label_3_label = Qt.QLabel(str(self._variable_qtgui_label_3_formatter(self.variable_qtgui_label_3)))
        self._variable_qtgui_label_3_tool_bar.addWidget(self._variable_qtgui_label_3_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_3_tool_bar, 2,1,1,1)

        self._variable_qtgui_label_2_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_2_formatter = None
        else:
          self._variable_qtgui_label_2_formatter = lambda x: x

        self._variable_qtgui_label_2_tool_bar.addWidget(Qt.QLabel('Algorithm'+": "))
        self._variable_qtgui_label_2_label = Qt.QLabel(str(self._variable_qtgui_label_2_formatter(self.variable_qtgui_label_2)))
        self._variable_qtgui_label_2_tool_bar.addWidget(self._variable_qtgui_label_2_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_2_tool_bar, 1,1,1,1)

        self._variable_qtgui_label_1_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_1_formatter = None
        else:
          self._variable_qtgui_label_1_formatter = lambda x: x

        self._variable_qtgui_label_1_tool_bar.addWidget(Qt.QLabel('Active channels'+": "))
        self._variable_qtgui_label_1_label = Qt.QLabel(str(self._variable_qtgui_label_1_formatter(self.variable_qtgui_label_1)))
        self._variable_qtgui_label_1_tool_bar.addWidget(self._variable_qtgui_label_1_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_1_tool_bar, 1,0,1,1)

        self._variable_qtgui_label_0_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_0_formatter = None
        else:
          self._variable_qtgui_label_0_formatter = lambda x: x

        self._variable_qtgui_label_0_tool_bar.addWidget(Qt.QLabel("<h2>Intelligent Dynamic User</h2> using {} for channel selection".format("uniform access" if use_uniform_selection else "AI")+": "))
        self._variable_qtgui_label_0_label = Qt.QLabel(str(self._variable_qtgui_label_0_formatter(self.variable_qtgui_label_0)))
        self._variable_qtgui_label_0_tool_bar.addWidget(self._variable_qtgui_label_0_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_0_tool_bar, 0,0,1,2)

        self.uhd_usrp_source_0 = uhd.usrp_source(
        	",".join((address, "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source_0.set_clock_source('external', 0)
        self.uhd_usrp_source_0.set_time_source('external', 0)
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0.set_time_unknown_pps(uhd.time_spec())
        self.uhd_usrp_source_0.set_center_freq(uhd.tune_request(center_freq,1.2e6), 0)
        self.uhd_usrp_source_0.set_gain(0, 0)
        self.uhd_usrp_source_0.set_antenna('RX2', 0)
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
        	",".join((address, "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0.set_clock_source('external', 0)
        self.uhd_usrp_sink_0.set_time_source('external', 0)
        self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0.set_time_unknown_pps(uhd.time_spec())
        self.uhd_usrp_sink_0.set_center_freq(uhd.tune_request(center_freq,1.2e6), 0)
        self.uhd_usrp_sink_0.set_gain(analog_gain, 0)
        self.uhd_usrp_sink_0.set_antenna('TX/RX', 0)
        self.qtgui_vector_sink_f_0 = qtgui.vector_sink_f(
            Nb_ch,
            0,
            1,
            "Channel",
            "",
            "Information by channels",
            4 # Number of inputs
        )
        self.qtgui_vector_sink_f_0.set_update_time(0.10)
        self.qtgui_vector_sink_f_0.set_y_axis(0, 1)
        self.qtgui_vector_sink_f_0.enable_autoscale(True)
        self.qtgui_vector_sink_f_0.enable_grid(True)
        self.qtgui_vector_sink_f_0.set_x_axis_units("")
        self.qtgui_vector_sink_f_0.set_y_axis_units("")
        self.qtgui_vector_sink_f_0.set_ref_level(0)

        labels = ['Trials', 'Successes', 'UCB indexes', 'Success rate', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 2, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "magenta", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(4):
            if len(labels[i]) == 0:
                self.qtgui_vector_sink_f_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_vector_sink_f_0.set_line_label(i, labels[i])
            self.qtgui_vector_sink_f_0.set_line_width(i, widths[i])
            self.qtgui_vector_sink_f_0.set_line_color(i, colors[i])
            self.qtgui_vector_sink_f_0.set_line_alpha(i, alphas[i])

        self._qtgui_vector_sink_f_0_win = sip.wrapinstance(self.qtgui_vector_sink_f_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_vector_sink_f_0_win, 4,1,1,1)
        self.qtgui_sink_x_1_1_0_0_2 = qtgui.sink_c(
        	FFT_Size, #fftsize
        	firdes.WIN_RECTANGULAR, #wintype
        	center_freq, #fc
        	samp_rate, #bw
        	"USRP_TX_SU  Sink after FFT", #name
        	False, #plotfreq
        	True, #plotwaterfall
        	False, #plottime
        	False, #plotconst
        )
        self.qtgui_sink_x_1_1_0_0_2.set_update_time(1.0/2)
        self._qtgui_sink_x_1_1_0_0_2_win = sip.wrapinstance(self.qtgui_sink_x_1_1_0_0_2.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_sink_x_1_1_0_0_2_win, 4,0,1,1)

        self.qtgui_sink_x_1_1_0_0_2.enable_rf_freq(True)



        self.qtgui_sink_x_1_1_0_0 = qtgui.sink_c(
        	FFT_Size, #fftsize
        	firdes.WIN_RECTANGULAR, #wintype
        	center_freq, #fc
        	samp_rate, #bw
        	"USRP_TX_SU  Sink before FFT", #name
        	False, #plotfreq
        	False, #plotwaterfall
        	False, #plottime
        	True, #plotconst
        )
        self.qtgui_sink_x_1_1_0_0.set_update_time(1.0/10)
        self._qtgui_sink_x_1_1_0_0_win = sip.wrapinstance(self.qtgui_sink_x_1_1_0_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_sink_x_1_1_0_0_win, 3,0,1,1)

        self.qtgui_sink_x_1_1_0_0.enable_rf_freq(True)



        self.qtgui_sink_x_1_1 = qtgui.sink_c(
        	FFT_Size, #fftsize
        	firdes.WIN_RECTANGULAR, #wintype
        	center_freq, #fc
        	samp_rate, #bw
        	"USRP_TX_SU  Source", #name
        	False, #plotfreq
        	True, #plotwaterfall
        	False, #plottime
        	False, #plotconst
        )
        self.qtgui_sink_x_1_1.set_update_time(1.0/10)
        self._qtgui_sink_x_1_1_win = sip.wrapinstance(self.qtgui_sink_x_1_1.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_sink_x_1_1_win, 3,1,1,1)

        self.qtgui_sink_x_1_1.enable_rf_freq(True)



        self.generator_SU_generator_SU_V6_0 = generator_SU.generator_SU_V6(Nb_ch, (Active_ch), Preambl_l, Packet_l, int(seconds_between_packets * samp_rate), real_part_data, imag_part_data, use_uniform_selection, use_thompson, short_message_to_send)
        self.fft_vxx_0_0 = fft.fft_vcc(FFT_Size, False, (window.rectangular(FFT_Size)), True, 1)
        self.fft_vxx_0 = fft.fft_vcc(FFT_Size, True, (window.rectangular(FFT_Size)), True, 1)
        self.blocks_vector_to_stream_0_1_0_0 = blocks.vector_to_stream(gr.sizeof_gr_complex*1, FFT_Size)
        self.blocks_vector_to_stream_0_1_0 = blocks.vector_to_stream(gr.sizeof_gr_complex*1, FFT_Size)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, FFT_Size)
        self.blocks_null_sink_0_0_1 = blocks.null_sink(gr.sizeof_gr_complex*FFT_Size)
        self.blocks_null_sink_0_0_0_0 = blocks.null_sink(gr.sizeof_gr_complex*FFT_Size)
        self.blocks_null_sink_0_0_0 = blocks.null_sink(gr.sizeof_gr_complex*FFT_Size)
        self.blocks_null_sink_0_0 = blocks.null_sink(gr.sizeof_gr_complex*FFT_Size)
        self.blocks_multiply_const_vxx_0_1 = blocks.multiply_const_vcc((digital_gain**2, ))
        self.blocks_multiply_const_vxx_0_0 = blocks.multiply_const_vcc((1.0/digital_gain if (digital_gain > 0) else 1.0, ))
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vcc((digital_gain, ))
        self.blocks_complex_to_real_4 = blocks.complex_to_real(Nb_ch)
        self.blocks_complex_to_real_3 = blocks.complex_to_real(Nb_ch)
        self.blocks_complex_to_real_2 = blocks.complex_to_real(Nb_ch)
        self.blocks_complex_to_real_1 = blocks.complex_to_real(Nb_ch)
        self.Renormalize_ack_Renormalize_ack_v3_0 = Renormalize_ack.Renormalize_ack_v3(Demod_threshold, Nb_ch, (Active_ch), Preambl_l/10)
        self.Check_ack_Check_ack_V2_0 = Check_ack.Check_ack_V2(Nb_ch, (Active_ch), Packet_l, 0.07, 0.01, False, real_part_data, imag_part_data)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.Check_ack_Check_ack_V2_0, 0), (self.generator_SU_generator_SU_V6_0, 0))
        self.connect((self.Renormalize_ack_Renormalize_ack_v3_0, 0), (self.Check_ack_Check_ack_V2_0, 0))
        self.connect((self.blocks_complex_to_real_1, 0), (self.qtgui_vector_sink_f_0, 0))
        self.connect((self.blocks_complex_to_real_2, 0), (self.qtgui_vector_sink_f_0, 1))
        self.connect((self.blocks_complex_to_real_3, 0), (self.qtgui_vector_sink_f_0, 2))
        self.connect((self.blocks_complex_to_real_4, 0), (self.qtgui_vector_sink_f_0, 3))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.qtgui_sink_x_1_1_0_0_2, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.uhd_usrp_sink_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.blocks_multiply_const_vxx_0_1, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_1, 0), (self.qtgui_sink_x_1_1, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.fft_vxx_0, 0))
        self.connect((self.blocks_vector_to_stream_0_1_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.blocks_vector_to_stream_0_1_0_0, 0), (self.qtgui_sink_x_1_1_0_0, 0))
        self.connect((self.fft_vxx_0, 0), (self.Renormalize_ack_Renormalize_ack_v3_0, 0))
        self.connect((self.fft_vxx_0_0, 0), (self.blocks_vector_to_stream_0_1_0, 0))
        self.connect((self.generator_SU_generator_SU_V6_0, 1), (self.blocks_complex_to_real_1, 0))
        self.connect((self.generator_SU_generator_SU_V6_0, 2), (self.blocks_complex_to_real_2, 0))
        self.connect((self.generator_SU_generator_SU_V6_0, 3), (self.blocks_complex_to_real_3, 0))
        self.connect((self.generator_SU_generator_SU_V6_0, 4), (self.blocks_complex_to_real_4, 0))
        self.connect((self.generator_SU_generator_SU_V6_0, 2), (self.blocks_null_sink_0_0, 0))
        self.connect((self.generator_SU_generator_SU_V6_0, 3), (self.blocks_null_sink_0_0_0, 0))
        self.connect((self.generator_SU_generator_SU_V6_0, 4), (self.blocks_null_sink_0_0_0_0, 0))
        self.connect((self.generator_SU_generator_SU_V6_0, 1), (self.blocks_null_sink_0_0_1, 0))
        self.connect((self.generator_SU_generator_SU_V6_0, 0), (self.blocks_vector_to_stream_0_1_0_0, 0))
        self.connect((self.generator_SU_generator_SU_V6_0, 0), (self.fft_vxx_0_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.blocks_multiply_const_vxx_0_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "USRP_TX_SU__v1__obj3")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_speed_up_factor(self):
        return self.speed_up_factor

    def set_speed_up_factor(self, speed_up_factor):
        self.speed_up_factor = speed_up_factor
        self.set_samp_rate(int(32000*self.speed_up_factor))
        self.set_variable_qtgui_label_3(self._variable_qtgui_label_3_formatter("Packet of {} second{}, every {} second{}".format(self.seconds_packet*self.speed_up_factor, "s" if (self.seconds_packet*self.speed_up_factor) > 1 else "", self.seconds_between_packets, "s" if self.seconds_between_packets > 1 else "")))
        self.set_seconds_packet(1.0 / float(self.speed_up_factor))

    def get_seconds_packet(self):
        return self.seconds_packet

    def set_seconds_packet(self, seconds_packet):
        self.seconds_packet = seconds_packet
        self.set_Packet_l(int(self.samp_rate * self.seconds_packet))
        self.set_variable_qtgui_label_3(self._variable_qtgui_label_3_formatter("Packet of {} second{}, every {} second{}".format(self.seconds_packet*self.speed_up_factor, "s" if (self.seconds_packet*self.speed_up_factor) > 1 else "", self.seconds_between_packets, "s" if self.seconds_between_packets > 1 else "")))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_Packet_l(int(self.samp_rate * self.seconds_packet))
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)
        self.qtgui_sink_x_1_1_0_0_2.set_frequency_range(self.center_freq, self.samp_rate)
        self.qtgui_sink_x_1_1_0_0.set_frequency_range(self.center_freq, self.samp_rate)
        self.qtgui_sink_x_1_1.set_frequency_range(self.center_freq, self.samp_rate)

    def get_idnumber(self):
        return self.idnumber

    def set_idnumber(self, idnumber):
        self.idnumber = idnumber
        self.set_use_uniform_selection(self.idnumber == 2)
        self.set_use_thompson(self.idnumber == 3)
        self.set_real_part_data(+1 if self.idnumber == 2 else -1)
        self.set_address("addr=192.168.10.104" if self.idnumber==1 else ("addr=192.168.10.109" if self.idnumber==2 else "addr=192.168.10.114"))
        self.set_Active_ch([1,5,9,12] if self.idnumber==1 else ([2,6,10,13] if self.idnumber==2 else [3,7,11,14]))

    def get_use_uniform_selection(self):
        return self.use_uniform_selection

    def set_use_uniform_selection(self, use_uniform_selection):
        self.use_uniform_selection = use_uniform_selection
        self.set_variable_qtgui_label_2(self._variable_qtgui_label_2_formatter(("Uniform access" if self.use_uniform_selection else ("Thompson Sampling" if self.use_thompson else "UCB")) + ("" if self.short_message_to_send == "" else ", sending '{}'".format(self.short_message_to_send)) + " and symbols " + str(self.real_part_data) + " +- " + str(self.imag_part_data) + "j"))

    def get_use_thompson(self):
        return self.use_thompson

    def set_use_thompson(self, use_thompson):
        self.use_thompson = use_thompson
        self.set_variable_qtgui_label_2(self._variable_qtgui_label_2_formatter(("Uniform access" if self.use_uniform_selection else ("Thompson Sampling" if self.use_thompson else "UCB")) + ("" if self.short_message_to_send == "" else ", sending '{}'".format(self.short_message_to_send)) + " and symbols " + str(self.real_part_data) + " +- " + str(self.imag_part_data) + "j"))

    def get_short_message_to_send(self):
        return self.short_message_to_send

    def set_short_message_to_send(self, short_message_to_send):
        self.short_message_to_send = short_message_to_send
        self.set_variable_qtgui_label_2(self._variable_qtgui_label_2_formatter(("Uniform access" if self.use_uniform_selection else ("Thompson Sampling" if self.use_thompson else "UCB")) + ("" if self.short_message_to_send == "" else ", sending '{}'".format(self.short_message_to_send)) + " and symbols " + str(self.real_part_data) + " +- " + str(self.imag_part_data) + "j"))

    def get_seconds_between_packets(self):
        return self.seconds_between_packets

    def set_seconds_between_packets(self, seconds_between_packets):
        self.seconds_between_packets = seconds_between_packets
        self.set_variable_qtgui_label_3(self._variable_qtgui_label_3_formatter("Packet of {} second{}, every {} second{}".format(self.seconds_packet*self.speed_up_factor, "s" if (self.seconds_packet*self.speed_up_factor) > 1 else "", self.seconds_between_packets, "s" if self.seconds_between_packets > 1 else "")))

    def get_real_part_data(self):
        return self.real_part_data

    def set_real_part_data(self, real_part_data):
        self.real_part_data = real_part_data
        self.set_variable_qtgui_label_2(self._variable_qtgui_label_2_formatter(("Uniform access" if self.use_uniform_selection else ("Thompson Sampling" if self.use_thompson else "UCB")) + ("" if self.short_message_to_send == "" else ", sending '{}'".format(self.short_message_to_send)) + " and symbols " + str(self.real_part_data) + " +- " + str(self.imag_part_data) + "j"))

    def get_imag_part_data(self):
        return self.imag_part_data

    def set_imag_part_data(self, imag_part_data):
        self.imag_part_data = imag_part_data
        self.set_variable_qtgui_label_2(self._variable_qtgui_label_2_formatter(("Uniform access" if self.use_uniform_selection else ("Thompson Sampling" if self.use_thompson else "UCB")) + ("" if self.short_message_to_send == "" else ", sending '{}'".format(self.short_message_to_send)) + " and symbols " + str(self.real_part_data) + " +- " + str(self.imag_part_data) + "j"))

    def get_digital_gain_default(self):
        return self.digital_gain_default

    def set_digital_gain_default(self, digital_gain_default):
        self.digital_gain_default = digital_gain_default
        self.set_digital_gain(self.digital_gain_default)

    def get_Packet_l(self):
        return self.Packet_l

    def set_Packet_l(self, Packet_l):
        self.Packet_l = Packet_l
        self.set_Preambl_l(int(self.Packet_l / 32))

    def get_Nb_ch(self):
        return self.Nb_ch

    def set_Nb_ch(self, Nb_ch):
        self.Nb_ch = Nb_ch
        self.set_FFT_Size(self.Nb_ch)

    def get_Active_ch(self):
        return self.Active_ch

    def set_Active_ch(self, Active_ch):
        self.Active_ch = Active_ch
        self.set_variable_qtgui_label_1(self._variable_qtgui_label_1_formatter(", ".join(["#{:d}".format(ch) for ch in self.Active_ch])))

    def get_variable_qtgui_label_3(self):
        return self.variable_qtgui_label_3

    def set_variable_qtgui_label_3(self, variable_qtgui_label_3):
        self.variable_qtgui_label_3 = variable_qtgui_label_3
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_3_label, "setText", Qt.Q_ARG("QString", str(self.variable_qtgui_label_3)))

    def get_variable_qtgui_label_2(self):
        return self.variable_qtgui_label_2

    def set_variable_qtgui_label_2(self, variable_qtgui_label_2):
        self.variable_qtgui_label_2 = variable_qtgui_label_2
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_2_label, "setText", Qt.Q_ARG("QString", str(self.variable_qtgui_label_2)))

    def get_variable_qtgui_label_1(self):
        return self.variable_qtgui_label_1

    def set_variable_qtgui_label_1(self, variable_qtgui_label_1):
        self.variable_qtgui_label_1 = variable_qtgui_label_1
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_1_label, "setText", Qt.Q_ARG("QString", repr(self.variable_qtgui_label_1)))

    def get_variable_qtgui_label_0(self):
        return self.variable_qtgui_label_0

    def set_variable_qtgui_label_0(self, variable_qtgui_label_0):
        self.variable_qtgui_label_0 = variable_qtgui_label_0
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_0_label, "setText", Qt.Q_ARG("QString", str(self.variable_qtgui_label_0)))

    def get_digital_gain(self):
        return self.digital_gain

    def set_digital_gain(self, digital_gain):
        self.digital_gain = digital_gain
        self._digital_gain_callback(self.digital_gain)
        self.blocks_multiply_const_vxx_0_1.set_k((self.digital_gain**2, ))
        self.blocks_multiply_const_vxx_0_0.set_k((1.0/self.digital_gain if (self.digital_gain > 0) else 1.0, ))
        self.blocks_multiply_const_vxx_0.set_k((self.digital_gain, ))

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.uhd_usrp_source_0.set_center_freq(uhd.tune_request(self.center_freq,1.2e6), 0)
        self.uhd_usrp_sink_0.set_center_freq(uhd.tune_request(self.center_freq,1.2e6), 0)
        self.qtgui_sink_x_1_1_0_0_2.set_frequency_range(self.center_freq, self.samp_rate)
        self.qtgui_sink_x_1_1_0_0.set_frequency_range(self.center_freq, self.samp_rate)
        self.qtgui_sink_x_1_1.set_frequency_range(self.center_freq, self.samp_rate)

    def get_analog_gain(self):
        return self.analog_gain

    def set_analog_gain(self, analog_gain):
        self.analog_gain = analog_gain
        self.uhd_usrp_sink_0.set_gain(self.analog_gain, 0)


    def get_address(self):
        return self.address

    def set_address(self, address):
        self.address = address

    def get_Preambl_l(self):
        return self.Preambl_l

    def set_Preambl_l(self, Preambl_l):
        self.Preambl_l = Preambl_l

    def get_FFT_Size(self):
        return self.FFT_Size

    def set_FFT_Size(self, FFT_Size):
        self.FFT_Size = FFT_Size

    def get_Demod_threshold(self):
        return self.Demod_threshold

    def set_Demod_threshold(self, Demod_threshold):
        self.Demod_threshold = Demod_threshold


def main(top_block_cls=USRP_TX_SU__v1__obj3, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
