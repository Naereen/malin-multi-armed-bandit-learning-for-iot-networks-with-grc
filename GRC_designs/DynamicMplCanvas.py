#!/usr/bin/env python
# -*- coding: utf8 -*-
# to embed generator_SU_V6_impl__plotdata.py in the Qt4 or Qt5 app
# Cf. https://matplotlib.org/examples/user_interfaces/embedding_in_qt4.html
# Cf. https://matplotlib.org/examples/user_interfaces/embedding_in_qt5.html

import matplotlib
# Make sure that we are using QT4
import socket
hostname = socket.gethostname()

# FIXME ugly fix to force Qt5 compatibility on my machine
use_qt5 = (hostname == 'jarvis')

if use_qt5:
    matplotlib.use('Qt5Agg')
else:
    matplotlib.use('Qt4Agg')

from matplotlib.backends import qt_compat
use_pyside = qt_compat.QT_API == qt_compat.QT_API_PYSIDE

if use_pyside:
    from PySide import QtGui, QtCore
else:
    try:
        if use_qt5:
            from PyQt5 import QtCore
            from PyQt5 import QtWidgets as QtGui
        else:
            raise RuntimeError
    except (ImportError, RuntimeError):
        from PyQt4 import QtGui, QtCore

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from os import path
from time import sleep

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

matplotlib.rcParams.update({'font.size': 8})


def getpalette(nb):
    return sns.hls_palette(nb + 1)[:nb]


LINES = [
    ("Number of Trials", int),
    ("Successes", int),
    ("UCB Indexes", float),
    ("Success Rates", float),
]


# ACTIVE_CH = [2,4,6,8]
ACTIVE_CH = [1,2,3,4,5,6,7,8]


def ordered_values(data, lines=LINES):
    values = []
    for line in lines:
        try:
            values.append(data[line[0]])
        except KeyError:
            print("{} is not found in data = {}".format(line[0], data))
    return values


class MyDynamicMplCanvas4(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent=None, width=7, height=7, dpi=100, Nb_ch=10, idnumber=1, Active_ch=ACTIVE_CH):
        self.Nb_ch = Nb_ch
        self.idnumber = idnumber
        self.Active_ch = Active_ch
        self.filepath = path.join("..", "GRC_designs", "generator_SU_V6_impl_cc_data_file_{}.txt".format(idnumber))
        self.olddata, self.data = None, None

        self.colors = getpalette(4)
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.fig.patch.set_facecolor('#EFEBE7' if use_qt5 else '#DADAFF')
        self.ax1 = self.fig.add_subplot(2, 2, 1)
        self.ax2 = self.fig.add_subplot(2, 2, 2)
        self.ax3 = self.fig.add_subplot(2, 2, 3)
        self.ax4 = self.fig.add_subplot(2, 2, 4)
        self.compute_initial_figure()

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(100)

    def compute_initial_figure(self):
        title = "IoT Object #{}".format(self.idnumber)
        self.fig.suptitle(title)
        activeChannelNum = len(self.Active_ch)
        # ax1
        self.rect1 = self.ax1.bar(self.Active_ch, [0]*activeChannelNum, color=self.colors[0])
        self.ax1.grid(True)
        self.ax1.set_title(LINES[0][0] + ", total = 0")
        self.ax1.set_xlim(0, self.Nb_ch)
        self.ax1.set_ylim(bottom=0, top=2)
        self.ax1.set_xticklabels([])
        # self.ax2
        self.rect2 = self.ax2.bar(self.Active_ch, [0]*activeChannelNum, color=self.colors[1])
        self.ax2.grid(True)
        self.ax2.set_title(LINES[1][0] + ", total = 0")
        self.ax2.set_xlim(0, self.Nb_ch)
        self.ax2.set_ylim(bottom=0, top=2)
        self.ax2.set_xticklabels([])
        # self.ax3
        self.rect3 = self.ax3.bar(self.Active_ch, [0]*activeChannelNum, color=self.colors[2])
        self.ax3.grid(True)
        self.ax3.set_title(LINES[2][0] + ", max = ?")
        self.ax3.set_xlim(0, self.Nb_ch)
        self.ax3.set_ylim(0, 1)
        self.ax3.set_xlabel("Active channels")
        # self.ax4
        self.rect4 = self.ax4.bar(self.Active_ch, [0]*activeChannelNum, color=self.colors[3])
        self.ax4.grid(True)
        self.ax4.set_title(LINES[3][0] + ", mean = 0")
        self.ax4.set_xlim(0, self.Nb_ch)
        self.ax4.set_ylim(0, 1)
        self.ax4.set_xlabel("Active channels")

    def update_figure(self):
        self.olddata = self.data
        data = self.read_data()
        ordered_data = ordered_values(data)
        if len(ordered_data) > 0 and ordered_data is not None and self.olddata != ordered_data:
            # print("\nNew data, updating the plot...")
            self.add_data(ordered_data)
            self.draw()

    def read_data(self, lines=LINES, sep=':'):
        try:
            data = dict()
            with open(self.filepath, 'r') as f:
                for line, (title, typecast) in zip(f, lines):
                    this_data = [typecast(text.replace(',', '.')) for text in line[:-1].split(sep)]
                    data[title] = this_data
            # print(data)
            return data
        except KeyError:
            return None

    def add_data(self, datas):
        fig, axes, rects = self.fig, [self.ax1, self.ax2, self.ax3, self.ax4], [self.rect1, self.rect2, self.rect3, self.rect4]
        mean_success_rate = np.sum(datas[1]) / np.sum(datas[0])
        for ax, rect, data in zip(axes, rects, datas):
            # print("For rect =", rect, "using data =", data)  # DEBUG
            for r, h in zip(rect, data):
                r.set_height(h)
            title = ax.get_title()
            if 'mean' in title.lower():
                # FIXED compute this smartly
                if np.sum(datas[0]) > 0:
                    mean_success_rate = np.sum(datas[1]) / float(np.sum(datas[0]))
                else:
                    mean_success_rate = np.mean(data)
                if 'rate' in title.lower():
                    title = "{}= {:.0%}".format(title.split('=')[0], mean_success_rate)
                else:
                    title = "{}= {:.3g}".format(title.split('=')[0], mean_success_rate)
            elif 'total' in title.lower():
                title = "{}= {:.3g}".format(title.split('=')[0], np.sum(data))
            elif 'max' in title.lower():
                title = "{}= #{}".format(title.split('=')[0], self.Active_ch[np.argmax(data)])
            ax.set_title(title)
            if max(data) <= 1:
                ax.set_ylim(0, 1)
            else:
                ax.set_ylim(0, 1 + max(data))
