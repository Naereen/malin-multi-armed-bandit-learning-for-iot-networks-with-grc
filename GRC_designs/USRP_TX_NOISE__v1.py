#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: IoT Traffic Generator
# Author: Remi Bonnefoi and Lilian Besson
# Description: Random interferring IoT traffic generator
# Generated: Thu Jul 26 16:13:40 2018
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from optparse import OptionParser
import Blocs_C
import sip
import sys
import time
from gnuradio import qtgui


class USRP_TX_NOISE__v1(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "IoT Traffic Generator")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("IoT Traffic Generator")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "USRP_TX_NOISE__v1")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Variables
        ##################################################
        self.speed_up_factor = speed_up_factor = 2
        self.Nb_ch = Nb_ch = 16
        self.samp_rate = samp_rate = int(32000*speed_up_factor)
        self.nb_noise_by_second = nb_noise_by_second = int(4 *speed_up_factor)
        self.digital_gain_default = digital_gain_default = 0.03
        self.Occupancy_rate = Occupancy_rate = [15,15,15, 0, 10,10,10, 0, 2,2,2, 1,1,1]
        self.Active_ch = Active_ch = list(range(1, Nb_ch-1))
        self.variable_qtgui_label_1 = variable_qtgui_label_1 = ''
        self.variable_qtgui_label_0_0_0 = variable_qtgui_label_0_0_0 = int(nb_noise_by_second / float(speed_up_factor))
        self.variable_qtgui_label_0_0 = variable_qtgui_label_0_0 = ", ".join(["{:.0%}".format(rate/100.0) for rate in Occupancy_rate])
        self.variable_qtgui_label_0 = variable_qtgui_label_0 = ", ".join(["#{:d}".format(ch) for ch in Active_ch])
        self.digital_gain = digital_gain = digital_gain_default
        self.center_freq = center_freq = 433.5e6
        self.analog_gain = analog_gain = 1
        self.address = address = "addr=192.168.10.110"
        self.Noise_packet_length = Noise_packet_length = int(samp_rate / nb_noise_by_second)
        self.FFT_Size = FFT_Size = Nb_ch

        ##################################################
        # Blocks
        ##################################################
        _digital_gain_check_box = Qt.QCheckBox('Enable IoT traffic')
        self._digital_gain_choices = {True: digital_gain_default, False: 0}
        self._digital_gain_choices_inv = dict((v,k) for k,v in self._digital_gain_choices.iteritems())
        self._digital_gain_callback = lambda i: Qt.QMetaObject.invokeMethod(_digital_gain_check_box, "setChecked", Qt.Q_ARG("bool", self._digital_gain_choices_inv[i]))
        self._digital_gain_callback(self.digital_gain)
        _digital_gain_check_box.stateChanged.connect(lambda i: self.set_digital_gain(self._digital_gain_choices[bool(i)]))
        self.top_grid_layout.addWidget(_digital_gain_check_box, 3,0,1,1)
        self._variable_qtgui_label_1_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_1_formatter = None
        else:
          self._variable_qtgui_label_1_formatter = lambda x: x

        self._variable_qtgui_label_1_tool_bar.addWidget(Qt.QLabel("<center><h2>IoT Traffic Generator</h2>Random occupation in active channels<br>Demo for ICT 2018<br>By Lilian Besson and Remi Bonnefoi<br><img scale='50%' src='/home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/GRC_designs/CS.png'></center>"+": "))
        self._variable_qtgui_label_1_label = Qt.QLabel(str(self._variable_qtgui_label_1_formatter(self.variable_qtgui_label_1)))
        self._variable_qtgui_label_1_tool_bar.addWidget(self._variable_qtgui_label_1_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_1_tool_bar, 0,0,1,1)

        self._variable_qtgui_label_0_0_0_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_0_0_0_formatter = None
        else:
          self._variable_qtgui_label_0_0_0_formatter = lambda x: x

        self._variable_qtgui_label_0_0_0_tool_bar.addWidget(Qt.QLabel('Number of messages by second'+": "))
        self._variable_qtgui_label_0_0_0_label = Qt.QLabel(str(self._variable_qtgui_label_0_0_0_formatter(self.variable_qtgui_label_0_0_0)))
        self._variable_qtgui_label_0_0_0_tool_bar.addWidget(self._variable_qtgui_label_0_0_0_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_0_0_0_tool_bar, 4,0,1,1)

        self._variable_qtgui_label_0_0_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_0_0_formatter = None
        else:
          self._variable_qtgui_label_0_0_formatter = lambda x: x

        self._variable_qtgui_label_0_0_tool_bar.addWidget(Qt.QLabel('Occupancy rates'+": "))
        self._variable_qtgui_label_0_0_label = Qt.QLabel(str(self._variable_qtgui_label_0_0_formatter(self.variable_qtgui_label_0_0)))
        self._variable_qtgui_label_0_0_tool_bar.addWidget(self._variable_qtgui_label_0_0_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_0_0_tool_bar, 2,0,1,1)

        self._variable_qtgui_label_0_tool_bar = Qt.QToolBar(self)

        if None:
          self._variable_qtgui_label_0_formatter = None
        else:
          self._variable_qtgui_label_0_formatter = lambda x: x

        self._variable_qtgui_label_0_tool_bar.addWidget(Qt.QLabel('Active channels'+": "))
        self._variable_qtgui_label_0_label = Qt.QLabel(str(self._variable_qtgui_label_0_formatter(self.variable_qtgui_label_0)))
        self._variable_qtgui_label_0_tool_bar.addWidget(self._variable_qtgui_label_0_label)
        self.top_grid_layout.addWidget(self._variable_qtgui_label_0_tool_bar, 1,0,1,1)

        self.uhd_usrp_sink_0 = uhd.usrp_sink(
        	",".join((address, "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0.set_clock_source('external', 0)
        self.uhd_usrp_sink_0.set_time_source('external', 0)
        self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0.set_time_unknown_pps(uhd.time_spec())
        self.uhd_usrp_sink_0.set_center_freq(uhd.tune_request(center_freq,1.2e6), 0)
        self.uhd_usrp_sink_0.set_gain(analog_gain, 0)
        self.uhd_usrp_sink_0.set_antenna('TX/RX', 0)
        self.qtgui_sink_x_1_1_0 = qtgui.sink_c(
        	FFT_Size, #fftsize
        	firdes.WIN_RECTANGULAR, #wintype
        	center_freq, #fc
        	samp_rate, #bw
        	"USRP_TX_PU Sink", #name
        	False, #plotfreq
        	True, #plotwaterfall
        	False, #plottime
        	False, #plotconst
        )
        self.qtgui_sink_x_1_1_0.set_update_time(1.0/10)
        self._qtgui_sink_x_1_1_0_win = sip.wrapinstance(self.qtgui_sink_x_1_1_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_sink_x_1_1_0_win, 5,0,1,1)

        self.qtgui_sink_x_1_1_0.enable_rf_freq(True)



        self.fft_vxx_0_0 = fft.fft_vcc(FFT_Size, False, (window.rectangular(FFT_Size)), True, 1)
        self.blocks_vector_to_stream_0_1_0 = blocks.vector_to_stream(gr.sizeof_gr_complex*1, FFT_Size)
        self.blocks_multiply_const_vxx_0_0 = blocks.multiply_const_vcc((0.035, ))
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vcc((digital_gain, ))
        self.Blocs_C_Generator_V5_c_0 = Blocs_C.Generator_V5_c(FFT_Size, (Active_ch), (Occupancy_rate), 0, Noise_packet_length, 1)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.Blocs_C_Generator_V5_c_0, 0), (self.fft_vxx_0_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_multiply_const_vxx_0_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.uhd_usrp_sink_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.qtgui_sink_x_1_1_0, 0))
        self.connect((self.blocks_vector_to_stream_0_1_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.fft_vxx_0_0, 0), (self.blocks_vector_to_stream_0_1_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "USRP_TX_NOISE__v1")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_speed_up_factor(self):
        return self.speed_up_factor

    def set_speed_up_factor(self, speed_up_factor):
        self.speed_up_factor = speed_up_factor
        self.set_samp_rate(int(32000*self.speed_up_factor))
        self.set_variable_qtgui_label_0_0_0(self._variable_qtgui_label_0_0_0_formatter(int(self.nb_noise_by_second / float(self.speed_up_factor))))
        self.set_nb_noise_by_second(int(4 *self.speed_up_factor))

    def get_Nb_ch(self):
        return self.Nb_ch

    def set_Nb_ch(self, Nb_ch):
        self.Nb_ch = Nb_ch
        self.set_FFT_Size(self.Nb_ch)
        self.set_Active_ch(list(range(1, self.Nb_ch-1)))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_Noise_packet_length(int(self.samp_rate / self.nb_noise_by_second))
        self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)
        self.qtgui_sink_x_1_1_0.set_frequency_range(self.center_freq, self.samp_rate)

    def get_nb_noise_by_second(self):
        return self.nb_noise_by_second

    def set_nb_noise_by_second(self, nb_noise_by_second):
        self.nb_noise_by_second = nb_noise_by_second
        self.set_Noise_packet_length(int(self.samp_rate / self.nb_noise_by_second))
        self.set_variable_qtgui_label_0_0_0(self._variable_qtgui_label_0_0_0_formatter(int(self.nb_noise_by_second / float(self.speed_up_factor))))

    def get_digital_gain_default(self):
        return self.digital_gain_default

    def set_digital_gain_default(self, digital_gain_default):
        self.digital_gain_default = digital_gain_default
        self.set_digital_gain(self.digital_gain_default)

    def get_Occupancy_rate(self):
        return self.Occupancy_rate

    def set_Occupancy_rate(self, Occupancy_rate):
        self.Occupancy_rate = Occupancy_rate
        self.set_variable_qtgui_label_0_0(self._variable_qtgui_label_0_0_formatter(", ".join(["{:.0%}".format(rate/100.0) for rate in self.Occupancy_rate])))

    def get_Active_ch(self):
        return self.Active_ch

    def set_Active_ch(self, Active_ch):
        self.Active_ch = Active_ch
        self.set_variable_qtgui_label_0(self._variable_qtgui_label_0_formatter(", ".join(["#{:d}".format(ch) for ch in self.Active_ch])))

    def get_variable_qtgui_label_1(self):
        return self.variable_qtgui_label_1

    def set_variable_qtgui_label_1(self, variable_qtgui_label_1):
        self.variable_qtgui_label_1 = variable_qtgui_label_1
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_1_label, "setText", Qt.Q_ARG("QString", str(self.variable_qtgui_label_1)))

    def get_variable_qtgui_label_0_0_0(self):
        return self.variable_qtgui_label_0_0_0

    def set_variable_qtgui_label_0_0_0(self, variable_qtgui_label_0_0_0):
        self.variable_qtgui_label_0_0_0 = variable_qtgui_label_0_0_0
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_0_0_0_label, "setText", Qt.Q_ARG("QString", str(self.variable_qtgui_label_0_0_0)))

    def get_variable_qtgui_label_0_0(self):
        return self.variable_qtgui_label_0_0

    def set_variable_qtgui_label_0_0(self, variable_qtgui_label_0_0):
        self.variable_qtgui_label_0_0 = variable_qtgui_label_0_0
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_0_0_label, "setText", Qt.Q_ARG("QString", repr(self.variable_qtgui_label_0_0)))

    def get_variable_qtgui_label_0(self):
        return self.variable_qtgui_label_0

    def set_variable_qtgui_label_0(self, variable_qtgui_label_0):
        self.variable_qtgui_label_0 = variable_qtgui_label_0
        Qt.QMetaObject.invokeMethod(self._variable_qtgui_label_0_label, "setText", Qt.Q_ARG("QString", repr(self.variable_qtgui_label_0)))

    def get_digital_gain(self):
        return self.digital_gain

    def set_digital_gain(self, digital_gain):
        self.digital_gain = digital_gain
        self._digital_gain_callback(self.digital_gain)
        self.blocks_multiply_const_vxx_0.set_k((self.digital_gain, ))

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.uhd_usrp_sink_0.set_center_freq(uhd.tune_request(self.center_freq,1.2e6), 0)
        self.qtgui_sink_x_1_1_0.set_frequency_range(self.center_freq, self.samp_rate)

    def get_analog_gain(self):
        return self.analog_gain

    def set_analog_gain(self, analog_gain):
        self.analog_gain = analog_gain
        self.uhd_usrp_sink_0.set_gain(self.analog_gain, 0)


    def get_address(self):
        return self.address

    def set_address(self, address):
        self.address = address

    def get_Noise_packet_length(self):
        return self.Noise_packet_length

    def set_Noise_packet_length(self, Noise_packet_length):
        self.Noise_packet_length = Noise_packet_length

    def get_FFT_Size(self):
        return self.FFT_Size

    def set_FFT_Size(self, FFT_Size):
        self.FFT_Size = FFT_Size


def main(top_block_cls=USRP_TX_NOISE__v1, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
