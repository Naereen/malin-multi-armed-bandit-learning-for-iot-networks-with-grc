#!/bin/bash
# start_all_blocks_in_tmux_panes.sh for https://bitbucket.org/scee_ietr/malin-multi-arm-bandit-learning-for-iot-networks-with-grc
# GPLv3 Licensed
#
# Script to start a new tab in a tmux session, launch the various Python blocks in panes

# Use https://bitbucket.org/lbesson/bin/src/master/.color.sh to add colors in Bash scripts
[ -f ~/.color.sh ] && . ~/.color.sh

echo -e "${yellow}Starting '${black}start_all_blocks_in_tmux_panes.sh'${reset} ..."

if [ -L "${BASH_SOURCE[0]}" ]; then
    # We have a symlink... how to deal with it?
    cd "$( dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"
else
    cd "$( dirname "${BASH_SOURCE[0]}" )"
fi;

# XXX assume runing inside a tmux session
if [ "X${TMUX}" = "X" ]; then
    echo -e "${red}This script ${black}${0}${red} has to be run inside a tmux session.${reset}"
    exit 1
fi

# Reference tmux man page (eg. https://linux.die.net/man/1/tmux)
# tmux new-window -a -n 'MALIN' "tmux split-window -d \"python2 -i ./USRP_TX_SU__v1.sh | tee ./USRP_TX_SU__v1.log\" ; tmux split-window -h \"python2 -i ./USRP_TX_PU__v1.sh | tee ./USRP_TX_PU__v1.log\" ; python2 -i ./USRP_RX_BTS__v1.sh | tee ./USRP_RX_BTS__v1.log"
tmux new-window -a -n 'MALIN' "tmux split-window -d \"sleep 1 ; python2 -i ./USRP_TX_PU__v1.sh\" ; tmux split-window -h \"sleep 10 ; python2 -i ./USRP_RX_BTS__v1.sh\" ; sleep 20 ; python2 -i ./USRP_TX_SU__v1.sh"
