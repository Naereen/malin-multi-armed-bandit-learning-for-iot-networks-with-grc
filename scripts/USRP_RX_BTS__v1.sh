#!/usr/bin/env bash
clear
cd /home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/GRC_designs/
notify-send --icon="/home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/scripts/icons/internet-radio-new.png" "Starting Gateway..."
time ./USRP_RX_BTS__v1.py
notify-send --icon="/home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/scripts/icons/internet-radio-new.png" "Done for Gateway..."
