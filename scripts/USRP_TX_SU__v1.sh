#!/usr/bin/env bash
clear
cd /home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/GRC_designs/
notify-send --icon="/home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/scripts/icons/gnome-phone-manager.svg" "Starting IoT Object #1..."
time ./USRP_TX_SU__v1.py
notify-send --icon="/home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/scripts/icons/gnome-phone-manager.svg" "Done for IoT Object #1..."
