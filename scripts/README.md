# Scripts

This folder contains a few bash scripts to launch easily our demonstration.

- This [`Makefile`](Makefile) allows to launch all the blocks easily,
- Some desktop (`.desktop`) files to launch the components of the experiment with a simple click,
- The [`icons/`](icons/) folder contains some icons used by the desktop files,
- Some shell scripts (`.sh`) to launch the [Python scripts](../GRC_designs/),
- Some other shell scripts were just used as experiments ([generator_SU_V6_impl__plotdata.py](generator_SU_V6_impl__plotdata.py) as a command line version of [this Qt UI visualisation tool](../GRC_designs/DynamicMplCanvas.py), [watch-message-file-and-notify.sh](watch-message-file-and-notify.sh), [notify-from-file-content.sh](notify-from-file-content.sh), [start_all_blocks_in_tmux_panes.sh](start_all_blocks_in_tmux_panes.sh), [watch-file.sh](watch-file.sh)).

## Copyright
Copyright (C) 2017-2018  Rémi Bonnefoi, Lilian Besson

GPLv3 Licensed ([file LICENSE](../LICENSE)).

