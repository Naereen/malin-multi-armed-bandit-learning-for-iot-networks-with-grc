#!/usr/bin/env bash
clear
cd /home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/GRC_designs/
notify-send --icon="/home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/scripts/icons/radiotray.png" "Starting IoT Traffic Generator..."
time ./USRP_TX_NOISE__v1.py
notify-send --icon="/home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/scripts/icons/radiotray.png" "Done for IoT Traffic Generator..."
