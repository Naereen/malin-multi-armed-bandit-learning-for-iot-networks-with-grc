#!/usr/bin/env python
# -*- coding: utf8 -*-
"""
Constantly plot the data saved from a GNU Radio block...
"""

from os import path
from pprint import pprint
from time import sleep

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import matplotlib
matplotlib.rcParams.update({'font.size': 8})


def getpalette(nb):
    return sns.hls_palette(nb + 1)[:nb]


DEFAULTFILEPATH = path.join("..", "GRC_designs", "generator_SU_V6_impl_cc_data_file_1.txt")
# DEFAULTFILEPATH = "/tmp/test.txt"  # DEBUG

LINES = [
    ("Number of Trials", int),
    ("Number of Success", int),
    ("UCB Indexes", float),
    ("Success Rates", float),
]

def ordered_values(data, lines=LINES):
    values = []
    for line in lines:
        values.append(data[line[0]])
    return values


NB_CH = 10
ACTIVECHANNELS = [2, 4, 6, 8]

def createfig(number="1", Nb_ch=NB_CH, activeChannels=ACTIVECHANNELS):
    colors = getpalette(4)
    # row and column sharing
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)  #, sharex='col', sharey='row')
    title = "IoT Object #{}".format(number)
    fig.suptitle(title)
    fig.canvas.set_window_title(title)
    activeChannelNum = len(activeChannels)
    # ax1
    rect1 = ax1.bar(activeChannels, [0]*activeChannelNum, color=colors[0])
    ax1.grid(True)
    ax1.set_title(LINES[0][0] + ", total = 0")
    ax1.set_xlim(0, Nb_ch)
    ax1.set_ylim(bottom=0, top=2)
    ax1.set_xticklabels([])
    # ax2
    rect2 = ax2.bar(activeChannels, [0]*activeChannelNum, color=colors[1])
    ax2.grid(True)
    ax2.set_title(LINES[1][0] + ", total = 0")
    ax2.set_xlim(0, Nb_ch)
    ax2.set_ylim(bottom=0, top=2)
    ax2.set_xticklabels([])
    # ax3
    rect3 = ax3.bar(activeChannels, [0]*activeChannelNum, color=colors[2])
    ax3.grid(True)
    ax3.set_title(LINES[2][0] + ", max = ?")
    ax3.set_xlim(0, Nb_ch)
    ax3.set_ylim(0, 1)
    ax3.set_xlabel("Active channels")
    # ax4
    rect4 = ax4.bar(activeChannels, [0]*activeChannelNum, color=colors[3])
    ax4.grid(True)
    ax4.set_title(LINES[3][0] + ", mean = 0")
    ax4.set_xlim(0, Nb_ch)
    ax4.set_ylim(0, 1)
    ax4.set_xlabel("Active channels")
    # show and return
    plt.interactive(True)
    plt.show()
    return fig, [ax1, ax2, ax3, ax4], [rect1, rect2, rect3, rect4]


def readata(filepath=DEFAULTFILEPATH, lines=LINES, sep=':'):
    try:
        data = dict()
        with open(filepath, 'r') as f:
            for line, (title, typecast) in zip(f, lines):
                this_data = [typecast(text.replace(',', '.')) for text in line[:-1].split(sep)]
                data[title] = this_data
        pprint(data)
        return data
    except KeyError:
        return None


def adddata(fig, axes, rects, datas, activeChannels=ACTIVECHANNELS):
    for ax, rect, data in zip(axes, rects, datas):
        # print("For rect =", rect, "using data =", data)  # DEBUG
        for r, h in zip(rect, data):
            r.set_height(h)
        title = ax.get_title()
        if 'mean' in title.lower():
            if 'rate' in title.lower():
                title = "{}= {:.0%}".format(title.split('=')[0], np.mean(data))
            else:
                title = "{}= {:.3g}".format(title.split('=')[0], np.mean(data))
        elif 'total' in title.lower():
            title = "{}= {:.3g}".format(title.split('=')[0], np.sum(data))
        elif 'max' in title.lower():
            title = "{}= #{}".format(title.split('=')[0], activeChannels[np.argmax(data)])
        ax.set_title(title)
        if max(data) <= 1:
            ax.set_ylim(0, 1)
        else:
            ax.set_ylim(0, 1 + max(data))
    fig.show()
    fig.canvas.draw()
    fig.canvas.flush_events()


def main(number=1, filepath=DEFAULTFILEPATH):
    fig, axes, rects = createfig(number=number)
    olddata, data = None, None
    while True:
        olddata = data
        data = readata(filepath=filepath)
        if data is not None and olddata != data:
            print("\nNew data, updating the plot...")
            adddata(fig, axes, rects, ordered_values(data))
        sleep(1)


if __name__ == '__main__':
    from sys import argv
    number = argv[1] if len(argv) > 1 else "1"
    filepath = argv[2] if len(argv) > 2 else DEFAULTFILEPATH
    while True:
        # try:
            main(number, filepath)
        # except (AttributeError, KeyboardInterrupt, ValueError):
        #     break
        # except Exception as e:
        #     print("Saw an exception", e, "but it is ignored...")  # DEBUG
