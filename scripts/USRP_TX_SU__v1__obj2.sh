#!/usr/bin/env bash
clear
cd /home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/GRC_designs/
notify-send --icon="/home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/scripts/icons/gnome-phone-manager.svg" "Starting IoT Object #2..."
time ./USRP_TX_SU__v1__obj2.py
notify-send --icon="/home/projet_etudiant/malin-multi-arm-bandit-learning-for-iot-networks-with-grc/scripts/icons/gnome-phone-manager.svg" "Done for IoT Object #2..."
