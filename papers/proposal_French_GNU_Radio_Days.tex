\documentclass[a4paper,10pt,conference,twocolumn]{article}

% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathrsfs}
\usepackage{bbm}
\usepackage{todonotes}
\usepackage{color}
\usepackage{pdfpages}

\usepackage{graphicx}
\usepackage{fullpage}
\usepackage{multicol}
\usepackage{url}
\usepackage{caption}
\newenvironment{Figure}
{\par\medskip\noindent\minipage{\linewidth}}
{\endminipage\par\medskip}
\pagestyle{empty}

\setcounter{tocdepth}{3}

\title{\Large \bf
        GNU Radio Implementation and Demonstration of MALIN:\\
        ``Multi-Arm bandits Learning for Internet-of-things Networks''
}

\author{
        Lilian Besson%
        \thanks{CentraleSup\'elec (campus of Rennes), IETR, SCEE Team,
                Avenue de la Boulaie - CS $47601$, $35576$ Cesson-S\'evign\'e, France
                {Email: \tt lilian.besson, remi.bonnefoi at centralesupelec.fr}}
        \thanks{Univ. Lille 1, CNRS, Inria, SequeL Team,
                UMR $9189$ - CRIStAL,  $59000$ Lille, France
                {Email: \tt lilian.besson at inria.fr}}%
        \and R{\'e}mi Bonnefoi\footnotemark[1]
        \and Christophe Moy% <-this % stops a space
        \thanks{University Rennes, CNRS, IETR -- UMR 6164,
                $35000$ Rennes, France
                {Email: \tt christophe.moy at univ-rennes1.fr}}%
}

\graphicspath{{pictures/}}

\begin{document}

\date{}

\maketitle

\pagenumbering{arabic}
\setcounter{page}{1}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}

% \footnotetext{Alphabetically ordered as authors contributed equally.}% <-this % stops a space

% One to two page extended abstract (PDF format), describing the contribution itself and the desired presentation format (oral, poster and/or demo).
% FIXME Deadline : 20 May 2018
%
% We implement a small network, with one gateway, interfering traffic and several intelligent (learning) objects, communicating in a wireless ALOHA-based protocol with slotted frequency.

We implement\footnote{We propose a demonstration for the French GNU Radio days, in reply of \texttt{gnuradio-fr-18.sciencesconf.org} $2018$.}
an IoT network the following way: one gateway, one or several intelligent (learning) objects, embedding the proposed solution,
and a traffic generator that emulates radio interferences from many other objects.
Intelligent objects communicate with the gateway with a wireless ALOHA-based protocol with no specific overhead for learning needs.
%
We model the network access as a discrete sequential decision making, and using the framework and algorithms from Multi-Armed Bandit (MAB) learning, we show that intelligent objects can improve their access to the network by using load complexity and decentralized algorithms, such as UCB and Thompson Sampling.

\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Objectives and importance}

Unlicensed bands are more and more used and considered for mobile and LAN communication standards (WiFi, LTE-U), and for Internet of Things (IoT) standards for short-range (ZigBee, Z-Wave, Bluetooth) and long-range (LoRaWAN, SIGFOX, Ingenu, Weightless) communications \cite{Centenaro16}.
% This heavy use of unlicensed bands will cause performance drop, and could even compromise IoT promises.
%
Efficient Medium Access (MAC) policies allow objects to avoid interfering traffic and can significantly reduce the spectrum contention problem in unlicensed bands.
As the objects battery life is a key constraint of IoT networks,
this leads to IoT protocols using as low signaling overhead as possible and simple ALOHA-based mechanisms.
In this demo, we evaluate Multi-Armed Bandits algorithms \cite{bubeck2012regret}, used in combination with a pure ALOHA-based protocol.
We consider the Upper-Confidence Bound (UCB) \cite{Auer}, and the Thompson-Sampling (TS) algorithms \cite{Thompson33}.
Both algorithms have already been applied with success, for Opportunistic Spectrum Access \cite{Jouini} and recently for multi-users Cognitive Radio problems \cite{BessonALT18}.

% MAB learning has already been proposed in Cognitive Radio (CR) \cite{Haykin}, and in particular, for sensing-based Dynamic Spectrum Access (DSA) in licensed bands \cite{Jouini}.
% Recently, TS and UCB{} algorithms have been used for improving the spectrum access in (unlicensed) WiFi networks \cite{Toldov}, and the UCB{} algorithm was used in a unlicensed and frequency- and time-slotted IoT network \cite{Bonnefoi}.
% Many recent works show that MAB algorithms work well for real-world radio signal.
% However, even with only one dynamic user using the learning algorithm, the background traffic or the traffic of the other objects is never really stationary or \emph{i.i.d} (independent and identically distributed).
% In recent works like \cite{Bonnefoi}, several objects are using bandit algorithms, and the assumptions made by the stochastic bandit algorithms are not satisfied: as several agents learn simultaneously, their behavior is neither stationary nor \emph{i.i.d}.
% As far as we know, we provide the first study to confirm robustness of the use of stochastic bandit algorithms for decision making in IoT networks with a large number of intelligent objects in the network, which makes the environment not stationary at all, violating the hypothesis required for mathematical proofs of bandit algorithms convergence and efficiency.

This demo aims at assessing the potential gain of learning algorithms in IoT scenarios.
% % even when the number of intelligent objects in the network increases, and the stochastic hypothesis is more and more questionable.
% To do that, we suppose an IoT network made of two types of objects: static objects that use only one channel (fixed in time), and dynamic objects that can choose the channel for each of their transmissions. Static objects form an interfering traffic, which could have been generated by objects using other standards as well.
% We first evaluate the probability of collision if dynamic objects randomly select channels (naive approach), and if a centralized controller optimally distribute them in channels (ideal approach).
% Then, these reference scenarios allow to evaluate the performance of UCB and TS algorithms in a decentralized network, in terms of successful communication rate, as it reflects the network efficiency.
% We show that these algorithms have near-optimal performance, even when the proportion of end-objects increases and the interfering traffic from other objects becomes less and less stochastic.
%
In a simple wireless network, consisting of one gateway (radio access point), and a certain interfering background traffic assumed to be stationary,
some dynamic intelligent objects (end-user or autonomous objects), try to access the network, with a low-overhead protocol.
To simulate networks designed for the Internet of Things (IoT), we consider a protocol with no sensing, no repetition of uplink messages, and where the gateway is in charge of sending back an acknowledgement, after some fixed-time delay, to any object who succeeded in sending successfully an uplink packet.
%
By considering a small number of wireless channels ($10$) and one PHY layer configuration (\emph{i.e.}, modulation, waveform, etc), and in case of a non-uniform traffic in the different channels,
the object can improve their usage of the network if they are able to \emph{learn} on the fly the best channels to use (\emph{i.e.}, the most vacant).

Following our recent work \cite{Bonnefoi17}, we propose to model this problem as Non-Stationary\footnote{Non-stationarity comes from the precense of more than one dynamic object.} Multi-Armed Bandit (MAB), and suggest to use low-cost algorithms, focusing on two well-known algorithms: a frequentist one (UCB, Upper Confidence Bounds) and a Bayesian one (Thompson Sampling).
%
We use a TestBed designed in 2017 by our team SCEE \cite{Bodinier17}, containing different USRP cards \cite{USRPDocumentation}, controlled by a single laptop using GNU Radio \cite{GNURadioDocumentation},
and where the intelligence of each object corresponds to a learning algorithm, implemented as a GNU Radio block \cite{GNURadioCompanionDocumentation} and written in Python or C++.

Our demo can modify dynamically the background traffic in a configurable number of channels, and reset at any time the channel selection algorithms of a End-Users with low duty-cycle, with a second one running the naive uniform access for comparison.
This allows to check that in case of uniform traffic, there is nothing to learn and the intelligent object do not reduce their successful communication rate in comparison to the naive object,
and in case of stationary non-uniform traffic, the MAB learning algorithms indeed help to increase the global efficiency of the network by improving the success rate of the intelligent object.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Demo layout}

% Demo layout, picture, or diagram

Figure~1 shows the layout of our demonstration.
Figures~2, 3 and 4 show the GNU Radio Companion (GRC, \cite{GNURadioCompanionDocumentation}) schemes corresponding respectively to the \emph{interfering traffic} generator (in charge of generating a random stationary traffic in each channel, with a fixed duty-cycle), the \emph{gateway} (in charge of listening in each channel, detecting incoming messages and replying with an acknowledgement), and one or more objects (dynamic device in charge of emitting in a sequentially chosen channel, receiving an acknowledgement and using the statistics and its learning algorithm to decide which channel to use next).
%
In our demo, the objects try to communicate with the gateway. This communication is hindered by some interfering traffic. This interfering traffic is supposed to be unevenly distributed in channels and is generated by a single USRP. If the gateway receives a message transmitted by an intelligent object and can decode it, it acknowledges it by sending a short ACK message.

The basic blocks (sink, source, FFT) are builtin GRC, but all the other blocks (\verb+demodulator+, \verb+send_ack+, \verb+check_ack+ for the gateway, \verb+generator+ for the interfering traffic, and \verb+renormalize_ack+ and \verb+generator_SU+ for the objects) are written in C++ for this demonstration.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{List of equipment for demo}

We require a large-screen TV, but we will bring everything else.
Our demo is developed using a large Testbed, as showed in Figure~5,
but it can be transported by using only one laptop, connected to a portable switch, to control the different USRP cards and an octoclock for synchronisation.

Our demo operates on the $433.5\,\mathrm{M}\mathrm{Hz}$ band, with a bandwidth of $2\,\mathrm{M}\mathrm{Hz}$, at low power consumption.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Conclusion}

% A conclusion section is not required. Although a conclusion may review the main points of the paper, do not replicate the abstract as the conclusion. A conclusion might elaborate on the importance of the work or suggest applications and extensions.

Possible extensions of this work include:
considering more dynamic objects,
implementing a real-world IoT communication protocol (like the LoRaWAN standard),
and studying the interference in case of other gateways located nearby.

\addtolength{\textheight}{-12cm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Acknowledgment}

The authors acknowledge the work of two CentraleSup{\'e}lec students,
Cl{\'e}ment Barras and Th{\'e}o Vanneuville, as we took inspiration in the GNU Radio
code they wrote during their project in Spring 2017.
% Théo Vanneuville, Clément Barras

% \begin{footnotesize}  % XXX?
This work is supported by the French National Research Agency (ANR), under the projects SOGREEN (grant coded: \emph{N ANR-14-CE28-0025-02}), by R\'egion Bretagne, France, by the CNRS, under the PEPS project BIO, by the French Ministry of Higher Education and Research (MENESR) and ENS Paris-Saclay.
% \end{footnotesize}  % XXX?


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% ---------- Bibliography ----------
\begin{footnotesize}
\bibliographystyle{ieeetr}
\bibliography{biblio_RIoT}
\end{footnotesize}

% \begin{footnotesize}  % XXX ?
    \emph{Note}: the code of our demo is for GNU Radio \cite{GNURadioDocumentation}
    and GNU Radio Companion \cite{GNURadioCompanionDocumentation},
    and is open-sourced under the GPLv3 License,
    on Bitbucket at: \verb|https://goo.gl/y6kKH2|.
    % Note: the source code for this article is itself open-sourced, under the MIT License, at XXX.
% \end{footnotesize}  % XXX ?

\includepdf[pages={1}]{figures.pdf}

\end{document}