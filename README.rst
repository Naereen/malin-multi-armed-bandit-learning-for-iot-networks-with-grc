MALIN: *M*\ ulti-*A*\ rmed bandit *L*\ earning for *Internet* of things *N*\ etworks with GRC
===========================================================================================

This repository contains the
`Python <http://gnuradio.org/redmine/projects/gnuradio/wiki/Guided_Tutorial_GNU_Radio_in_Python>`__
and C++ code for our `GNU Radio <http://gnuradio.org/>`__ demo entitled
"MALIN: Multi-Armed bandit Learning for Internet of things Networks with GRC".

We implemented three components using GNU Radio and GNU Radio companion:

1. An IoT traffic generator (left), simulate a traffic in the active
   channels, with a varying occupation rate between the channels. In the
   example below, rates of 50%, 25%, 10% and 2% are used. We can check
   on the left waterfall that channel 4 is almost always free, for
   instance.
2. An intelligent dynamic object (middle), which sends a message of 1
   second every 4 seconds, in one of the active channels. It then waits
   for an acknowledgement, sent by the gateway, to know if its message
   was successful or not. It can dynamically change channels, and its
   goal is to maximize its success rate. The object uses `multi-armed
   bandit <https://en.wikipedia.org/wiki/Multi-armed_bandit>`__
   algorithms to learn the mean occupancy rate of each channel (here,
   [UCB1]). We can see in the example below that it used mostly channels
   4 and 3, to maximize the efficiency of its communication with the
   gateway.
3. An IoT gateway (right), which listens for messages from the user(s),
   and when it receives a message, if it not corrupted or with a small
   corruption, it replies one second later with an acknowledgement (on
   the same channel).

    |docs/Gateway_Noise_Object1__UI_2.png| Figure 1: left shows the IoT
    traffic generator, middle shows the dynamic object learning to
    maximize its throughput (with the UCB1 algorithm), and right shows
    the IoT gateway.

Other details:

-  The demo uses 10 channels of 100 KHz bandwidth at the central
   frequency of 433.5 MHz, and only the channels [2,4,6,8] are used for
   the demo to have a clear separation between channels (see figure
   above).
-  3 USRP cards (by Ettus Research) are used to send and emit. They are
   synchronized in time and frequency using an Octoclock.
-  We can add a second object #2 to the system, for instance using a
   uniformly random access, to demonstrate that its total throughput is
   less than object #1 which uses a learning approach to find the "best"
   channels (ie, the most free ones).

--------------

Presentation of this demo
-------------------------

We presented this demonstration at `the ICT 2018
conference <http://ict-2018.org/call-for-demos/>`__, in Saint-Malo
(France), in June 2018.

Publication
-----------

We are writing a conference paper based on this work, stay up-to date
and come back to this page in September 2018.

As of now, you can read `this paper presenting our demo <https://perso.crans.org/besson/articles/BBM__Demo_ICT_2018.pdf>`__.

--------------

Explanations
------------

-  See the `docs/ <docs/>`__ folder for explanation on how to use
   and launch this demonstration,
-  See the explanation paper in `papers/ <papers/>`__ (`this
   PDF <proposal_French_GNU_Radio_Days.pdf>`__), for explanations
   regarding the design and protocol, with references.

--------------

Copyright
---------

Copyright (C) 2017-2018 R�mi Bonnefoi, Lilian Besson, Christophe Moy.

GPLv3 Licensed (`file LICENSE <LICENSE>`__).

.. |docs/Gateway_Noise_Object1__UI_2.png| image:: docs/Gateway_Noise_Object1__UI_2.png

