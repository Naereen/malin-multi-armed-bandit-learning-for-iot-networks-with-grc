/* -*- c++ -*- */
/*
 * Copyright 2018 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_GENERATOR_SU_GENERATOR_SU_V6_H
#define INCLUDED_GENERATOR_SU_GENERATOR_SU_V6_H

#include <generator_SU/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
    namespace generator_SU {

        /*!
        * \brief <+description of block+>
        * \ingroup generator_SU
        *
        */
        class GENERATOR_SU_API generator_SU_V6 : virtual public gr::sync_block {
            public:
                typedef boost::shared_ptr<generator_SU_V6> sptr;

            /*!
            * \brief Return a shared_ptr to a new instance of generator_SU::generator_SU_V6.
            *
            * To avoid accidental use of raw pointers, generator_SU::generator_SU_V6's
            * constructor is in a private implementation
            * class. generator_SU::generator_SU_V6::make is the public interface for
            * creating new instances.
            */
            static sptr make(
                const int Nb_ch,
                const std::vector<int> Active_ch,
                const int Preamb_l,
                const int data_l,
                const int delay_between_packets,
                const float r_data,
                const float i_data,
                const bool is_uniform,
                const bool is_thompson,
                const std::string const_message
            );
        };

    } // namespace generator_SU
} // namespace gr

#endif /* INCLUDED_GENERATOR_SU_GENERATOR_SU_V6_H */

