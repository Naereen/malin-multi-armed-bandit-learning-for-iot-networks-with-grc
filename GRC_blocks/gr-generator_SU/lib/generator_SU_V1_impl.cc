/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "generator_SU_V1_impl.h"
#include <math.h>

namespace gr {
  namespace generator_SU {

    generator_SU_V1::sptr
    generator_SU_V1::make(int Nb_ch,std::vector<int> Active_ch, int Preamb_l,int data_l)
    {
      return gnuradio::get_initial_sptr
        (new generator_SU_V1_impl(Nb_ch, Active_ch, Preamb_l, data_l));
    }

    /*
     * The private constructor
     */
    generator_SU_V1_impl::generator_SU_V1_impl(int Nb_ch,std::vector<int> Active_ch,int Preamb_l,int data_l)
      : gr::sync_block("generator_SU_V1",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1, 1, Nb_ch * sizeof(gr_complex))),
                d_channelNum(Nb_ch),
                d_activeChannels(Active_ch),
                d_activeChannelNum(Active_ch.size()),
                d_preamLength(Preamb_l),
                d_dataLength(data_l),
                d_active_ch(0),
                d_count(0),
                d_sum_reward(new float[Active_ch.size()]),
                d_nb_selection(new int[Active_ch.size()])
    {
    set_output_multiple(128);
    
        for(int i=0;i<d_activeChannelNum;i++)
        {
                d_sum_reward[i] = 0;
                d_nb_selection[i] = 0;
        }
        
        d_nb_selection[0] = 1;
    }

    /*
     * Our virtual destructor.
     */
    generator_SU_V1_impl::~generator_SU_V1_impl()
    {
    }
    int
    generator_SU_V1_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      gr_complex *out = (gr_complex *) output_items[0];
      int tot_size = 156250;
      int time_UCB = 0;
      float UCB_index;
      float max_index = 0;
      
      
      for(int i=0;i<d_activeChannelNum;i++)
      {
        time_UCB += d_nb_selection[i];
      }
      //std::cout << "total size" << tot_size << std::endl; 
      //std::cout << "data length"<< d_dataLength << std::endl;
      // Do <+signal processing+>
        for(int i = 0; i <d_channelNum*noutput_items;i++)
        {
                out[i] = 0;
        }
      
        for(int i = 0; i < noutput_items; i++)
        {
            if(d_count<d_dataLength)
            {
                out[i*d_channelNum+d_activeChannels[d_active_ch]] = 0.5+0.5j;
            }
        
            
            if(d_count<tot_size-1)
            {
                d_count++;
            }
            else
            {
                d_count = 0;
                // fake UCB1 algorithm (without reward) in order to selective the active channel
                
                // Initialization
                if(time_UCB<d_activeChannelNum)
                {
                       d_active_ch  = time_UCB;
                }
                else
                {
                     for(int j = 0;j<d_activeChannelNum;j++)
                     {
                        UCB_index = d_sum_reward[j]/d_nb_selection[j] + sqrt(0.5*log(time_UCB)/d_nb_selection[j]);
                        
                        if(UCB_index>max_index)
                        {
                                max_index = UCB_index;
                                d_active_ch = j;
                                std::cout << "max index" <<max_index << std::endl;
                        }
                     }
                     
                }
                
                d_nb_selection[d_active_ch]++;

            }
      }
      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace generator_SU */
} /* namespace gr */

