# Copyright 2011,2012,2016 Free Software Foundation, Inc.
#
# This file is part of GNU Radio
#
# GNU Radio is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# GNU Radio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Radio; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.

########################################################################
# Setup library
########################################################################
include(GrPlatform) #define LIB_SUFFIX

include_directories(${Boost_INCLUDE_DIR})
link_directories(${Boost_LIBRARY_DIRS})

list(APPEND generator_SU_sources
    generator_SU_V1_impl.cc
    generator_SU_V2_impl.cc
    test_source_SU_V2_impl.cc
    generator_SU_V3_impl.cc
    generator_SU_V4_impl.cc
    generator_SU_V5_impl.cc
    generator_SU_V6_impl.cc
)

set(generator_SU_sources "${generator_SU_sources}" PARENT_SCOPE)
if(NOT generator_SU_sources)
	MESSAGE(STATUS "No C++ sources... skipping lib/")
	return()
endif(NOT generator_SU_sources)

add_library(gnuradio-generator_SU SHARED ${generator_SU_sources})
target_link_libraries(gnuradio-generator_SU ${Boost_LIBRARIES} ${GNURADIO_ALL_LIBRARIES})
set_target_properties(gnuradio-generator_SU PROPERTIES DEFINE_SYMBOL "gnuradio_generator_SU_EXPORTS")

if(APPLE)
    set_target_properties(gnuradio-generator_SU PROPERTIES
        INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib"
    )
endif(APPLE)

########################################################################
# Install built library files
########################################################################
include(GrMiscUtils)
GR_LIBRARY_FOO(gnuradio-generator_SU RUNTIME_COMPONENT "generator_SU_runtime" DEVEL_COMPONENT "generator_SU_devel")

########################################################################
# Build and register unit test
########################################################################
include(GrTest)

include_directories(${CPPUNIT_INCLUDE_DIRS})

list(APPEND test_generator_SU_sources
    ${CMAKE_CURRENT_SOURCE_DIR}/test_generator_SU.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/qa_generator_SU.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/qa_generator_SU_V4.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/qa_test_source_SU_V2.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/qa_generator_SU_V2.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/qa_generator_SU_V1.cc
)

add_executable(test-generator_SU ${test_generator_SU_sources})

target_link_libraries(
  test-generator_SU
  ${GNURADIO_RUNTIME_LIBRARIES}
  ${Boost_LIBRARIES}
  ${CPPUNIT_LIBRARIES}
  gnuradio-generator_SU
)

GR_ADD_TEST(test_generator_SU test-generator_SU)

########################################################################
# Print summary
########################################################################
message(STATUS "Using install prefix: ${CMAKE_INSTALL_PREFIX}")
message(STATUS "Building for version: ${VERSION} / ${LIBVER}")

