/* -*- c++ -*- */
/*
 * Copyright 2018 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_GENERATOR_SU_GENERATOR_SU_V6_IMPL_H
#define INCLUDED_GENERATOR_SU_GENERATOR_SU_V6_IMPL_H

#include <generator_SU/generator_SU_V6.h>
#include <gnuradio/random.h>
#include <boost/random.hpp>
#include <boost/random/beta_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>

namespace gr {
    namespace generator_SU {

    class generator_SU_V6_impl : public generator_SU_V6 {
        private:
            // Nothing to declare in this block.
            const int d_Nb_ch;
            const std::vector<int> d_activeChannels;
            const int d_activeChannelsNum;
            const int d_preamLength;
            const int d_dataLength;
            const int d_delay_between_packets;
            const float d_r_data;
            const float d_i_data;
            const bool d_is_uniform;
            const bool d_is_thompson;
            gr::random *d_rng;
            int d_time_UCB;
            int d_active_ch;
            int d_time_count_global_clock;
            int d_tot_size;
            int *d_sum_reward;
            int *d_nb_selection;
            std::string d_message;
            bool d_i_printed_about_this_message;
            bool *d_bool_vector_to_send;
            int d_bool_vector_offset;
            float *d_all_UCB_index;
            const char* d_filepath_to_save_data;
            const char* d_filepath_to_save_data_append;

        public:
            generator_SU_V6_impl(
                const int Nb_ch,
                const std::vector<int> Active_ch,
                const int Preamb_l,
                const int data_l,
                const int delay_between_packets,
                const float r_data,
                const float i_data,
                const bool is_uniform,
                const bool is_thompson,
                const std::string const_message
            );
            ~generator_SU_V6_impl();

            // Where all the action really happens
            int work(
                int noutput_items,
                gr_vector_const_void_star &input_items,
                gr_vector_void_star &output_items
            );

            int random_channel();
        };

    } // namespace generator_SU
} // namespace gr

#endif /* INCLUDED_GENERATOR_SU_GENERATOR_SU_V6_IMPL_H */
