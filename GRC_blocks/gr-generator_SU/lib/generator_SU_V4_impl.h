/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_GENERATOR_SU_GENERATOR_SU_V4_IMPL_H
#define INCLUDED_GENERATOR_SU_GENERATOR_SU_V4_IMPL_H

#include <generator_SU/generator_SU_V4.h>

namespace gr {
  namespace generator_SU {

    class generator_SU_V4_impl : public generator_SU_V4
    {
     private:
      // Nothing to declare in this block.
        int d_channelNum;
        std::vector<int> d_activeChannels;
        int d_activeChannelNum;
        int d_preamLength;
        int d_dataLength;
        std::vector<float> d_data_v;
        int d_active_ch;
        int d_count;
        float* d_sum_reward;
        int* d_nb_selection;
        bool d_reward_received;
     public:
      generator_SU_V4_impl(int Nb_ch,std::vector<int> Active_ch,int Preamb_l,int data_l, std::vector<float> data_v);
      ~generator_SU_V4_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace generator_SU
} // namespace gr

#endif /* INCLUDED_GENERATOR_SU_GENERATOR_SU_V4_IMPL_H */

