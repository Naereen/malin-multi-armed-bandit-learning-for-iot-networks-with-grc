INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_SEND_ACK Send_ack)

FIND_PATH(
    SEND_ACK_INCLUDE_DIRS
    NAMES Send_ack/api.h
    HINTS $ENV{SEND_ACK_DIR}/include
        ${PC_SEND_ACK_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    SEND_ACK_LIBRARIES
    NAMES gnuradio-Send_ack
    HINTS $ENV{SEND_ACK_DIR}/lib
        ${PC_SEND_ACK_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(SEND_ACK DEFAULT_MSG SEND_ACK_LIBRARIES SEND_ACK_INCLUDE_DIRS)
MARK_AS_ADVANCED(SEND_ACK_LIBRARIES SEND_ACK_INCLUDE_DIRS)

