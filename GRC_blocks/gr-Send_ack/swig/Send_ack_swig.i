/* -*- c++ -*- */

#define SEND_ACK_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "Send_ack_swig_doc.i"

%{
#include "Send_ack/Send_ack_v2.h"
%}

%include "Send_ack/Send_ack_v2.h"
GR_SWIG_BLOCK_MAGIC2(Send_ack, Send_ack_v2);
