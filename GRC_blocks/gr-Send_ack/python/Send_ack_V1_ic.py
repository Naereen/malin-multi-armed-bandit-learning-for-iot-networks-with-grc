#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from __future__ import print_function

import numpy as np
from gnuradio import gr


b_00, b_01, b_10, b_11 = -1-1j, -1+1j, 1-1j, 1+1j


FREQUENCE_AFFICHAGE = 1000

DEBUG = False
DEBUG = True


class send_ack_V1_ic(gr.sync_block):
    """
    Le block send_ack_V1_ic doit :

    - recevoir en entree un vecteur d'entier de taille Nb_ch, et sur les canaux actifs (la liste Active_ch), verifier quel canal devient actif (passer de 0 a 1 puis de nouveau a 0).
    - emettre en sortie un vecteur de complexe de taille Nb_ch, pour chaque canal avec 1+1j si le canal doit recevoir un acknowledgment (après Delay_before_ack instants, et pendant Ack_length instants), et 0 sinon.
    """
    def __init__(self, Nb_ch, Active_ch, Delay_before_ack, Ack_length):
        gr.sync_block.__init__(self,
            name="send_ack_V1_ic",
            in_sig=[(np.int8, Nb_ch)],
            out_sig=[(np.complex64, Nb_ch)]
        )

        # Quelques tests
        assert len(set(Active_ch)) == len(Active_ch), "Erreur : Active_ch contient deux fois le meme canal."
        assert tuple(sorted(Active_ch)) == tuple(Active_ch), "Erreur : Active_ch n'est pas trie dans l'ordre croissant."
        assert set(Active_ch) <= set(list(range(Nb_ch))), "Erreur : Active_ch contient des elements qui ne sont pas dans [0,...,Nb_ch]."

        # fixed attributes
        self.Nb_ch            = Nb_ch             #: int, nombre de canaux
        self.Active_ch        = Active_ch         #: vecteur de taille int, contenant les canaux actifs
        self.Delay_before_ack = Delay_before_ack  #: taille du preambule, ie, nombre d'instants a attendre avant d'envoyer l'acknowledgement
        self.Ack_length       = Ack_length        #: taille de l'acknowledgment

        # internal data
        self.attentes_debut   = np.zeros(Nb_ch, dtype=np.int)  #: compte pour chaque canal la duree d'attente au debut, avant l'ack
        self.durees_ack       = np.zeros(Nb_ch, dtype=np.int)  #: compte pour chaque canal la duree de chaque ack
        self.nb_ack_envoyes   = np.zeros(Nb_ch, dtype=np.int)  #: XXX bonus, pour des stats : compte le nombre d'ack dans chaque canal (en fait, le nombre de FIN d'ack)

        # XXX aucune idee de pourquoi c'est la dans le code des eleves
        # self.set_output_multiple(10)


    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]

        # s = np.sum(in0[:, self.Active_ch])
        # if s >= 1:
        #     where = np.array(self.Active_ch)[list(set(tuple(np.where(in0[:, self.Active_ch] >= 1)[1])))]
        #     print("\nTraitement d'un nouveau bloc, contenant", s, "symboles 1 sur les canaux actifs", where, "...")  # DEBUG

        for i, in0_i in enumerate(in0):
            # on doit produire un vecteur complexe de taille Nb_ch, plein de zeros
            # 1+j selon sur quel canal on veut emettre un acknowledgement
            out[i, :] = 0

            # maintenant in0_i est un vecteur entier de taille Nb_ch, plein de zeros
            # si une case est 1, on va declencher quelque chose

            # si un canal devrait partir, on va le generer pour Ack_length instants
            for canal in self.Active_ch:
                # d'abord, verifions que chaque case est soit 0 soit 1
                # assert in0_i[canal] == 0 or in0_i[canal] == 1, "Erreur: la {}eme valeur d'un vecteur d'entree in0_i = {} n'est ni 0 ni 1".format(canal, in0_i[canal])  # DEBUG

                # on cherche le 1 (peut-etre plusieurs, aucune raison de se restreindre ici)
                if in0_i[canal] >= 1:
                    if DEBUG: print("Detection dans le canal", canal, "et l'acknowledgment sera emis dans", self.Delay_before_ack, "instants (attente courante =", self.attentes_debut[canal], ")...")  # DEBUG
                    # on commencera donc a produire un ack sur ce canal, apres Delay_before_ack instants d'attente
                    self.attentes_debut[canal] = 2 + self.Delay_before_ack
                    # XXX on met un 2 + comme ca meme si Delay_before_ack est 0, a l'etape d'apres ceux a 1 vont partir

                # ensuite, on verifie quels acknowledgments doivent partir A CET INSTANT
                if self.attentes_debut[canal] == 1:
                    self.durees_ack[canal] = self.Ack_length
                    if DEBUG: print("Un acknowledgment va COMMENCER a etre emis dans le canal", canal, "pour", self.durees_ack[canal], "instants consecutifs")  # DEBUG

                # puis on verifie si un autre ack est encore en cours d'emission
                # on verifie quels acknowledgments sont encore en cours A CET INSTANT
                if self.durees_ack[canal] >= 1:  # en cours
                    # et enfin on produit cet ack
                    out[i, canal] = b_11
                    if DEBUG:
                        # if self.durees_ack[canal] % FREQUENCE_AFFICHAGE == 0:  # pas afficher trop
                        #     print("Emission d'un acknowledgment dans le canal", canal, "avec le symbole complexe", out[i, canal], "pour encore", self.durees_ack[canal], "instants...")  # DEBUG
                        if self.durees_ack[canal] <= 1:
                            self.nb_ack_envoyes[canal] += 1
                            print("... FIN de cet acknowledgement sur le canal", canal)  # DEBUG
                            print("    c'etait le", self.nb_ack_envoyes[canal], "eme a finir sur ce canal...")  # DEBUG

                # on decremente les temps d'attentes debut
                self.attentes_debut[canal] = max(0, self.attentes_debut[canal] - 1)
                # on decremente les duree des ack
                self.durees_ack[canal]     = max(0, self.durees_ack[canal] - 1)

                # if DEBUG:
                #     if max(self.attentes_debut[canal], self.durees_ack[canal]) >= 1:
                #         if max(self.attentes_debut[canal], self.durees_ack[canal]) % FREQUENCE_AFFICHAGE == 0:  # pas afficher trop
                #             print("> Actuellement, pour canal", canal, "attentes_debut =", self.attentes_debut[canal], "et durees_ack =", self.durees_ack[canal], "et nb_ack_envoyes =", self.nb_ack_envoyes[canal], "...")  # DEGUG

        return len(output_items[0])

