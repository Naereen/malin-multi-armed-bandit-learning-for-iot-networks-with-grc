/* -*- c++ -*- */
/*
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CHECK_ACK_CHECK_ACK_V2_CC_H
#define INCLUDED_CHECK_ACK_CHECK_ACK_V2_CC_H

#include <Send_ack/Send_ack_v2.h>

namespace gr {
    namespace Send_ack {
        class Send_ack_v2_ic : public Send_ack_v2 {
            private:
                // Nothing to declare in this block.
                const int d_Nb_ch;
                const std::vector<int> d_activeChannels;
                const int d_activeChannelNum;  // not an input
                const int d_Delay_before_ack;
                const int d_Ack_length;
                const int d_preamLength;
                // Declare here the internal variables!
                int *d_delay_before_start;
                int *d_duration_acks;
                int *d_nb_sent_acks;
                gr_complex *d_data_acks;
                bool *d_bool_vector_to_print;
                int d_bool_vector_offset;

            public:
                Send_ack_v2_ic(
                    const int Nb_ch,
                    const std::vector<int> Active_ch,
                    const int Delay_before_ack,
                    const int Ack_length,
                    const int Preamb_l
                );
                ~Send_ack_v2_ic();

                // Where all the action really happens
                int work(
                    int noutput_items,
                    gr_vector_const_void_star &input_items,
                    gr_vector_void_star &output_items
                );
        };
    } // namespace Send_ack
} // namespace gr

#endif /* INCLUDED_Send_ACK_Send_ACK_V2_CC_H */
