/* -*- c++ -*- */
/*
 * Copyright 2018 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "Send_ack_v2_ic.h"
#include <gnuradio/io_signature.h>

// Some messages should not be printed too much, only X timesteps where X = 1000
#define PRINT_ON_X_TIMESTEPS (10000)

// Turn this on by commenting (false) and uncommenting (true), for full debug output
#define DOYOUWANTODEBUG (true)
// #define DOYOUWANTODEBUG (false)

// Turn this on by commenting (false) and uncommenting (true), to force norm one of output or use +-1+-1j
// #define NORM_ONE_NOT_PM1J (true)
#define NORM_ONE_NOT_PM1J (false)

// Turn this on by commenting (false) and uncommenting (true), to force using the conjugate for ACK
#define USE_CONJUGATE_FOR_ACK (true)
// #define USE_CONJUGATE_FOR_ACK (false)

// constant preamble = +1+1j
#define CONSTANT_COMPLEX_PREAMBLE (1 + 1j)

// Complex constants...
// the number 1,2,3,4 comes from
// n = 1 + (1 + real(z)) + (1 + imag(z))/2
// First, the unnormalized values +-1+-1j
gr_complex unnormed_b_00_1 = -1-1j;
gr_complex unnormed_b_01_2 = -1+1j;
gr_complex unnormed_b_10_3 = +1-1j;
gr_complex unnormed_b_11_4 = +1+1j;
// Second, the normalized values +-1+-1j
gr_complex normed_b_00_1   = -0.7071067811 - 0.7071067811j;
gr_complex normed_b_01_2   = -0.7071067811 + 0.7071067811j;
gr_complex normed_b_10_3   =  0.7071067811 - 0.7071067811j;
gr_complex normed_b_11_4   =  0.7071067811 + 0.7071067811j;
// Third, the normalized or unnormalized values +-1+-1j
gr_complex b_00_1 = NORM_ONE_NOT_PM1J ? normed_b_00_1 : unnormed_b_00_1;
gr_complex b_01_2 = NORM_ONE_NOT_PM1J ? normed_b_01_2 : unnormed_b_01_2;
gr_complex b_10_3 = NORM_ONE_NOT_PM1J ? normed_b_10_3 : unnormed_b_10_3;
gr_complex b_11_4 = NORM_ONE_NOT_PM1J ? normed_b_11_4 : unnormed_b_11_4;
// Forth, the conjugate of the values +-1+-1j
gr_complex conj_b_00_1 = std::conj(b_00_1);
gr_complex conj_b_01_2 = std::conj(b_01_2);
gr_complex conj_b_10_3 = std::conj(b_10_3);
gr_complex conj_b_11_4 = std::conj(b_11_4);

// Last, the values or conjugate of the values +-1+-1j
// WARNING use conjugate of input!
// don't use conjugate of input!
gr_complex answer_1 = USE_CONJUGATE_FOR_ACK ? conj_b_00_1 : b_00_1 ;
gr_complex answer_2 = USE_CONJUGATE_FOR_ACK ? conj_b_01_2 : b_01_2 ;
gr_complex answer_3 = USE_CONJUGATE_FOR_ACK ? conj_b_10_3 : b_10_3 ;
gr_complex answer_4 = USE_CONJUGATE_FOR_ACK ? conj_b_11_4 : b_11_4 ;

// Map an integer input to a complex output
// WARNING use conjugate of input!
gr_complex map_int_input_to_complex_output(int input) {
    switch (input) {
        case 0:
            return 0+0j;
            break;
        case 1:
            return answer_1;
            break;
        case 2:
            return answer_2;
            break;
        case 3:
            return answer_3;
            break;
        case 4:
            return answer_4;
            break;
        default:
            std::cout << "WARNING: input " << input << " was not in {0,1,2,3,4} in function map_int_input_to_complex_output..." << std::endl;  // DEFAULT
            return 0+0j;
            break;
    }
}

int max_of_int(int x, int y) {
    return x >= y ? x : y;
}


/** This function takes a bool vector, and returns a 2D GPS position on Earth
 *  Example:
 *  $ binary_to_position(std::vector<bool> { true, false, true, true, false, false, false, true, true, true, false, true, false, true, true, false, false, false, false, true, true, false, true, false, true, false, true, false, false, true, true, false, false, false, false, true, false, true, false, false, true, true, false, true, true, false, true, false, false, true, false, false, false, false, true, true, false, false, false, true, false, true })
 *  {48.6516678, -2.0214016}
*/
std::vector<double> binary_to_position(std::vector<bool> ret) {
    std::vector<double> xy_pos;
    // default values
    xy_pos.push_back(48.6516678);
    xy_pos.push_back(-2.0214016);

    long xy = 0L;
    long current_offset = 1L;
    std::reverse(ret.begin(), ret.end());
    for(int i = 0; i < ret.size(); i++) {
        if (ret[i]) {
            xy += current_offset;
        }
        std::cout << "xy = " << xy << " and current_offset = " << current_offset << std::endl;  // DEBUG
        current_offset <<= 1;  // *2
    }
    // std::cout << "xy = " << xy << std::endl;  // DEBUG

    long offset = 10000000L;
    // get x, y from long integer xy
    long x = xy % (180L * offset);
    // std::cout << "x = " << x << std::endl;  // DEBUG
    long y = (xy - x) / (180L * offset);
    // std::cout << "y = " << y << std::endl;  // DEBUG

    // build double from x, y
    double x_pos = (double) (((double) x) / ((double) offset) - 90.0f);
    // std::cout << "x_pos = " << x_pos << std::endl;  // DEBUG
    double y_pos = (double) (((double) y) / ((double) offset) - 180.0f);
    // std::cout << "y_pos = " << y_pos << std::endl;  // DEBUG
    // save values in the vector
    xy_pos[0] = x_pos;
    xy_pos[1] = y_pos;
    return xy_pos;
}


/** This function takes a bool vector, and returns a string
 *  Example:
 *  $ binary_to_string(std::vector<bool> { false, true, true, true, false, false, false, false, true, true, false, false, false, true, false, true, true, false, false, false, false, false, true, true, false, false, true, false, true, false, true, false, true, false, false, true, false, false, false, false, true, true, true, false, false, true, false, false, true, false, true, false, false, false, true, true })
 *  "#ICT2018"
*/
std::string binary_to_string(std::vector<bool> ret) {
    std::reverse(ret.begin(), ret.end());
    std::string message = "";
    char this_char = 0;
    int current_offset;

    int chunk_size = 7;
    // std::cout << "chunk_size = " << chunk_size << std::endl;  // DEBUG
    assert((ret.size() % chunk_size) == 0);
    int total_chunk_number = ret.size() / chunk_size;
    // std::cout << "total_chunk_number = " << total_chunk_number << std::endl;  // DEBUG

    for (int chunk_number = 0; chunk_number < total_chunk_number; chunk_number++) {
        // std::cout << "chunk_number = " << chunk_number << std::endl;  // DEBUG
        // reinitialize
        this_char = 0;
        current_offset = 1;
        // read 7 bits
        for (int i = 0; i < chunk_size; i++) {
            // std::cout << "i = " << i << " int of this_char = " << (int (this_char)) << " current_offset = " << current_offset << std::endl;  // DEBUG
            if (ret[chunk_number*chunk_size + i]) {
                // std::cout << "This bit was true at position = " << (chunk_number*chunk_size + i) << " so we add the integer " << (int (current_offset)) << " to the current this_char..." << std::endl;  // DEBUG
                this_char += current_offset;
            }
            current_offset <<= 1;
        }
        // std::cout << "this_char = " << this_char << " int of this_char = " << (int (this_char)) << std::endl;  // DEBUG
        message.push_back(this_char);
    }
    return message;
}


// max size of the bool vector used to send our message
#define MAX_SIZE_OF_BOOL_VECTOR (32)


/*
    Le block send_ack_v2_ic doit :

    - recevoir en entree un vecteur d'entier de taille Nb_ch, et sur les canaux actifs (la liste Active_ch), verifier quel canal devient actif (passer de 0 a 1 puis de nouveau a 0).
    - emettre en sortie un vecteur de complexe de taille Nb_ch, pour chaque canal avec 1+1j si le canal doit recevoir un acknowledgment (après Delay_before_ack instants, et pendant Ack_length instants), et 0 sinon.
*/
namespace gr {
    namespace Send_ack {

        Send_ack_v2::sptr
        Send_ack_v2::make(
            const int Nb_ch,
            const std::vector<int> Active_ch,
            const int Delay_before_ack,
            const int Ack_length,
            const int Preamb_l
        )
        {
            return gnuradio::get_initial_sptr(
                new Send_ack_v2_ic(
                        Nb_ch,
                        Active_ch,
                        Delay_before_ack,
                        Ack_length,
                        Preamb_l
                    )
                );
        }

        /*
        * The private constructor
        */
        Send_ack_v2_ic::Send_ack_v2_ic(
                const int Nb_ch,
                const std::vector<int> Active_ch,
                const int Delay_before_ack,
                const int Ack_length,
                const int Preamb_l
            ) :
            gr::sync_block(
                "Send_ack_v2",
                gr::io_signature::make(1, 1, Nb_ch * sizeof(int)),
                gr::io_signature::make(1, 1, Nb_ch * sizeof(gr_complex))
            ),
                d_Nb_ch(Nb_ch),
                d_activeChannels(Active_ch),
                d_activeChannelNum(Active_ch.size()),
                d_Delay_before_ack(Delay_before_ack),
                d_Ack_length(Ack_length),
                d_preamLength(Preamb_l),
                // attributes
                d_delay_before_start(new int[Active_ch.size()]),  // compte pour chaque canal la duree d'attente au debut, avant l'ack
                d_duration_acks(new int[Active_ch.size()]),  // compte pour chaque canal la duree de chaque ack
                d_nb_sent_acks(new int[Active_ch.size()]),  // XXX bonus, pour des stats : compte le nombre d'ack dans chaque canal (en fait, le nombre de FIN d'ack)
                d_data_acks(new gr_complex[Active_ch.size()]),  // pour chaque canal, garde en mémoire ce qu'on doit envoyer
                d_bool_vector_to_print(new bool[MAX_SIZE_OF_BOOL_VECTOR]),
                d_bool_vector_offset(-1)
            {
                // set_output_multiple(128);  // FIXME

                std::cout << std::endl << "Creating a new 'Send_ack_v2_ic.cc' block..." << std::endl;  // DEBUG
                int output_mutiple = sizeof(gr_complex) * d_Nb_ch;
                std::cout << std::endl << "With output_mutiple = " << output_mutiple << " ..." << std::endl;  // DEBUG
                set_output_multiple( output_mutiple );

                // DEBUG show input values of parameters
                std::cout << "d_Nb_ch: " << d_Nb_ch << std::endl;  // DEBUG
                std::cout << "d_activeChannelNum: " << d_activeChannelNum << std::endl;  // DEBUG
                for (int index_of_channel = 0; index_of_channel < d_activeChannelNum; index_of_channel++) {
                    std::cout << "    d_activeChannels[" << index_of_channel << "] = " << d_activeChannels[index_of_channel] << std::endl;  // DEBUG
                }
                std::cout << "d_Delay_before_ack: " << d_Delay_before_ack << std::endl;  // DEBUG
                std::cout << "d_Ack_length: " << d_Ack_length << std::endl;  // DEBUG
                std::cout << "d_preamLength: " << d_preamLength << std::endl;  // DEBUG

                std::cout << std::endl << std::endl << "Initializing all arrays with 0, 0, 0 and 0+0j respectively..." << std::endl;
                // WARNING be careful and put every array to default values
                for (int index_of_channel = 0; index_of_channel < d_activeChannelNum; index_of_channel++) {
                    d_delay_before_start[index_of_channel] = 0;
                    d_duration_acks[index_of_channel]      = 0;
                    d_nb_sent_acks[index_of_channel]       = 0;
                    d_data_acks[index_of_channel]          = 0+0j;
                }
            }

        /*
        * Our virtual destructor.
        */
        Send_ack_v2_ic::~Send_ack_v2_ic() {}

        /*
        * Signal processing
        */
        int Send_ack_v2_ic::work(
            int noutput_items,
            gr_vector_const_void_star &input_items,
            gr_vector_void_star &output_items
        ) {
            // std::cout << "Start Send_ack_v2_ic::work()..." << std::endl;

            const int *in = (const int *)input_items[0];
            gr_complex *out = (gr_complex *)output_items[0];

            int offset;
            int channel;

            for (int i = 0; i < noutput_items; i++) {

                // maintenant in[i * d_Nb_ch + XXX] est un vecteur entier de taille Nb_ch, plein de zeros
                // si une case est >= 1, on va declencher quelque chose

                // si un canal devrait partir, on va le generer pour Ack_length instants
                for (int index_of_channel = 0; index_of_channel < d_activeChannelNum; index_of_channel++) {
                    channel = d_activeChannels[index_of_channel];

                    offset = (i * d_Nb_ch + channel);

                    // on doit produire un vecteur complexe de taille Nb_ch, plein de zeros
                    // ou 1+j selon sur quel canal on veut emettre un acknowledgement
                    out[offset] = 0+0j;

                    // if (DOYOUWANTODEBUG && (i == 0)) {
                    //     std::cout << std::endl << "In channel # " << j << "/" << d_activeChannelNum << " which is channel " << channel << " the input was = " << in[offset] << " ..." << std::endl;  // DEBUG
                    // }

                    // on cherche le 1 (peut-etre plusieurs, aucune raison de se restreindre ici)
                    if (in[offset] >= 1) {
                        // on commencera donc a produire un ack sur ce canal, apres Delay_before_ack instants d'attente
                        d_delay_before_start[index_of_channel] = 1 + d_Delay_before_ack;
                        // XXX on met un 1 + comme ca meme si Delay_before_ack est 0, a l'etape d'apres ceux a 1 vont partir
                        // on stocke l'association de l'entrée avec le complexe a produire dans ce canal
                        d_data_acks[index_of_channel] = map_int_input_to_complex_output(in[offset]);

                        if (DOYOUWANTODEBUG) {
                            std::cout << std::endl << "Detection of input >= 1, = " << (int) in[offset] << " on channel " << channel << " and so an acknowledgment = " << d_data_acks[index_of_channel] << " will be emitted in " << d_Delay_before_ack << " time steps (current delay = " << d_delay_before_start[index_of_channel] << " )... " << std::endl;  // DEBUG
                        }
                    }

                    // ensuite, on verifie quels acknowledgments doivent partir A CET INSTANT
                    if (d_delay_before_start[index_of_channel] == 1) {
                        d_duration_acks[index_of_channel] = d_Ack_length;
                        std::cout << std::endl << std::endl << "An acknowledgment with data = " << d_data_acks[index_of_channel] << " will START to be sent on channel " << channel << " for " << d_duration_acks[index_of_channel] << " time steps" << std::endl;  // DEBUG
                    }

                    // puis on verifie si un autre ack est encore en cours d'emission
                    // on verifie quels acknowledgments sont encore en cours A CET INSTANT
                    if (d_duration_acks[index_of_channel] >= 1) {  // en cours
                        assert(d_delay_before_start[index_of_channel] <= 0);
                        // et enfin on produit cet ack
                        if (d_duration_acks[index_of_channel] >= (d_Ack_length - d_preamLength)) {  // encore preamble
                            out[offset] = CONSTANT_COMPLEX_PREAMBLE;
                        } else {
                            out[offset] = d_data_acks[index_of_channel];
                        }

                        if ((DOYOUWANTODEBUG) && (real(out[offset]) != 0)) {
                            if ((d_duration_acks[index_of_channel] % PRINT_ON_X_TIMESTEPS) == 0) {
                                std::cout << std::endl << "Sending an acknowledgment on channel " << channel << " with complex symbol " << out[offset] << " for again " << d_duration_acks[index_of_channel] << " time steps..." << std::endl;  // DEBUG
                            }
                        }

                        if (d_duration_acks[index_of_channel] <= 1) {
                            d_data_acks[index_of_channel] = 0+0j;
                            d_nb_sent_acks[index_of_channel] += 1;
                            std::cout << std::endl << std::endl << "... END of this acknowledgement on channel " << channel << " : it was the " << d_nb_sent_acks[index_of_channel] << " th to finish on this channel..." << std::endl;  // DEBUG
                        }
                    }

                    // on decremente les temps d'attentes debut
                    d_delay_before_start[index_of_channel] = max_of_int(0, d_delay_before_start[index_of_channel] - 1);
                    // on decremente les duree des ack
                    d_duration_acks[index_of_channel]      = max_of_int(0, d_duration_acks[index_of_channel]      - 1);

                    // si on ne doit plus produire cet ack et on plus attendre qu'il soit produit
                    // alors on efface la mémoire de la donnee qu'on devait produire
                    if (
                        (d_delay_before_start[index_of_channel] == 0)
                        &&
                        (d_duration_acks[index_of_channel] == 0)
                    ) {
                        d_data_acks[index_of_channel] = 0+0j;
                    }
                }
            }

            // Tell runtime system how many output items we produced.
            return noutput_items;
        }

    } /* namespace Send_ack */
} /* namespace gr */
