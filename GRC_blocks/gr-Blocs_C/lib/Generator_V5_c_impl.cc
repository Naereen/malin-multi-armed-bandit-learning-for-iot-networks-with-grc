/* -*- c++ -*- */
/*
 * Copyright 2017 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "Generator_V5_c_impl.h"
#include <ctime>
#include <gnuradio/io_signature.h>

// Turn this on by commenting (false) and uncommenting (true), for full debug output
#define DOYOUWANTODEBUG (true)
// #define DOYOUWANTODEBUG (false)

namespace gr {
    namespace Blocs_C {

        Generator_V5_c::sptr Generator_V5_c::make(
            int channelNum,
            std::vector<int> activeChannels,
            std::vector<float> occupancyRate,
            int preamLength,
            int dataLength,
            float digitalGain
        ) {
            return gnuradio::get_initial_sptr(
                new Generator_V5_c_impl(
                    channelNum,
                    activeChannels,
                    occupancyRate,
                    preamLength,
                    dataLength,
                    digitalGain
                )
            );
        }

        /*
        * The private constructor
        */
        Generator_V5_c_impl::Generator_V5_c_impl(
            int Nb_ch,
            std::vector<int> activeChannels,
            std::vector<float> occupancyRate,
            int preamLength,
            int dataLength,
            float digitalGain
        )
            : gr::sync_block(
                "Generator_V5_c",
                gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(1, 1, Nb_ch * sizeof(gr_complex))
            ),
            d_Nb_ch(Nb_ch),
            d_activeChannels(activeChannels),
            d_activeChannelNum(activeChannels.size()),
            d_gain(sqrt(2.0) * digitalGain),
            d_preamLength(preamLength),
            d_dataLength(dataLength),
            d_totalLength(preamLength + dataLength),
            d_remainingTicks(new int[activeChannels.size()]),
            d_proba(new float[activeChannels.size()])

        {
            // set_output_multiple(128);  // FIXME

            std::cout << std::endl << "Creating a new 'Generator_V5_c_impl.cc' block..." << std::endl;  // DEBUG
            int output_mutiple = sizeof(gr_complex) * d_Nb_ch;
            std::cout << std::endl << "With output_mutiple = " << output_mutiple << " ..." << std::endl;  // DEBUG
            set_output_multiple( output_mutiple );

            // DEBUG show input values of parameters
            std::cout << "d_Nb_ch: " << d_Nb_ch << std::endl;  // DEBUG
            std::cout << "d_activeChannelNum: " << d_activeChannelNum << std::endl;  // DEBUG
            for (int index_of_channel = 0; index_of_channel < d_activeChannelNum; index_of_channel++) {
                std::cout << "    d_activeChannels[" << index_of_channel << "] = " << d_activeChannels[index_of_channel] << std::endl;  // DEBUG
            }
            std::cout << "d_gain: " << d_gain << std::endl;  // DEBUG
            std::cout << "d_preamLength: " << d_preamLength << std::endl;  // DEBUG
            std::cout << "d_dataLength: " << d_dataLength << std::endl;  // DEBUG

            d_rng = new gr::random(((int) time(NULL)), 0, 2);

            for (int j = 0; j < d_activeChannelNum; j++) {
                d_remainingTicks[j] = 0;
            }

            // occupancyRate can be a constant vector
            if (occupancyRate.size() == 1) {
                for (int j = 0; j < d_activeChannelNum; j++) {
                    d_proba[j] = (occupancyRate[0] / 100) / (d_totalLength * (1 - (occupancyRate[0] / 100)));
                    std::cout << "d_proba[" << j << "] = " << d_proba[j] << " and occupancyRate = " << occupancyRate[0] << std::endl;
                }
            } else {
                for (int j = 0; j < d_activeChannelNum; j++) {
                    d_proba[j] = (occupancyRate[j] / 100) / (d_totalLength * (1 - (occupancyRate[j] / 100)));
                    std::cout << "d_proba[" << j << "] = " << d_proba[j] << std::endl;
                }
            }
        }
        /*
        * Our virtual destructor.
        */
        Generator_V5_c_impl::~Generator_V5_c_impl() {
            delete d_rng;
        }

        int Generator_V5_c_impl::random_value_int() {
            return d_rng->ran_int();
        }

        float Generator_V5_c_impl::random_value_1() {
            return d_rng->ran1();
        }

        /*
        * Signal processing
        */
        int Generator_V5_c_impl::work(
            int noutput_items,
            gr_vector_const_void_star &input_items,
            gr_vector_void_star &output_items
        ) {
            // std::cout << "Start Generator_V5_c_impl::work()..." << std::endl;

            // time_t t = time(NULL);
            gr_complex *out = (gr_complex *)output_items[0];

            int channel = -1;
            int offset = -1;
            float random_x;
            float random_y;

            for (int j = 0; j < d_activeChannelNum; j++) {
                channel = d_activeChannels[j];
                random_x = random_value_int();
                random_y = random_value_int();
                // std::cout << "For channel #" << channel << " random data x = " << random_x << " and y = " << random_y << " ..." << std::endl;  // DEBUG

                for (int i = 0; i < noutput_items; i++) {
                    offset = i * d_Nb_ch + channel;
                    if (d_remainingTicks[j] > 0) {
                        // Output the random data, for all the trame
                        out[offset] = d_gain * ((random_x - 0.5) + 1j*(random_y - 0.5));

                        if (DOYOUWANTODEBUG) {
                            if (d_remainingTicks[j] >= d_totalLength) {
                                std::cout << "Sending " << out[offset] << " in channel #" << channel << " (with gain = " << d_gain << ")..." << std::endl;  // DEBUG
                            }
                        }

                        d_remainingTicks[j] -= 1;
                    } else {
                        out[offset] = 0;
                        if (random_value_1() < d_proba[j]) {
                            d_remainingTicks[j] = d_totalLength;
                            if (DOYOUWANTODEBUG) {
                                std::cout << std::endl << "Channel = " << channel << " will produce data for " << d_remainingTicks[j] << " time steps " << std::endl;  // DEBUG
                            }
                        }
                    }
                }
            }

            // Tell runtime system how many output items we produced.
            return noutput_items;
        }

    } /* namespace Blocs_perso */
} /* namespace gr */
