/* -*- c++ -*- */
/*
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_BLOCS_C_DEMODULATOR_V3_CCC_IMPL_H
#define INCLUDED_BLOCS_C_DEMODULATOR_V3_CCC_IMPL_H

#include <Blocs_C/Demodulator_V3_ccc.h>

namespace gr {
    namespace Blocs_C {

        class Demodulator_V3_ccc_impl : public Demodulator_V3_ccc
        {
            private:
                // Nothing to declare in this block.
                const int d_channelNum;
                const int d_windowLength;
                std::vector<float> d_thresholdsV;
                float* d_thresholds;
                float* d_valuesArray;
                float* d_meanArray;
                int* d_indexArray;

            public:
                Demodulator_V3_ccc_impl(
                    const int channelNum,
                    const int windowLength,
                    std::vector<float> thresholds,
                    bool moving
                    );
                ~Demodulator_V3_ccc_impl();

                gr_complex projection(gr_complex a);

                // Where all the action really happens
                int work(
                    int noutput_items,
                    gr_vector_const_void_star &input_items,
                    gr_vector_void_star &output_items
                );
                };

    } // namespace Blocs_C
} // namespace gr

#endif /* INCLUDED_BLOCS_C_DEMODULATOR_V3_CCC_IMPL_H */

