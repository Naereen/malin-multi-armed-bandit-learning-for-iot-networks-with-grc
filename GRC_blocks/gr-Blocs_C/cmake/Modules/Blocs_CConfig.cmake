INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_BLOCS_C Blocs_C)

FIND_PATH(
    BLOCS_C_INCLUDE_DIRS
    NAMES Blocs_C/api.h
    HINTS $ENV{BLOCS_C_DIR}/include
        ${PC_BLOCS_C_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    BLOCS_C_LIBRARIES
    NAMES gnuradio-Blocs_C
    HINTS $ENV{BLOCS_C_DIR}/lib
        ${PC_BLOCS_C_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(BLOCS_C DEFAULT_MSG BLOCS_C_LIBRARIES BLOCS_C_INCLUDE_DIRS)
MARK_AS_ADVANCED(BLOCS_C_LIBRARIES BLOCS_C_INCLUDE_DIRS)

