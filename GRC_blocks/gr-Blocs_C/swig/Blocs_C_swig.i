/* -*- c++ -*- */

#define BLOCS_C_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "Blocs_C_swig_doc.i"

%{
#include "Blocs_C/Generator_V5_c.h"
#include "Blocs_C/Demodulator_V3_ccc.h"
%}


%include "Blocs_C/Generator_V5_c.h"
GR_SWIG_BLOCK_MAGIC2(Blocs_C, Generator_V5_c);

%include "Blocs_C/Demodulator_V3_ccc.h"
GR_SWIG_BLOCK_MAGIC2(Blocs_C, Demodulator_V3_ccc);
