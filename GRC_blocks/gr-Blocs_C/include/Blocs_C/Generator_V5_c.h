/* -*- c++ -*- */
/*
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_BLOCS_C_GENERATOR_V5_C_H
#define INCLUDED_BLOCS_C_GENERATOR_V5_C_H

#include <Blocs_C/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
    namespace Blocs_C {

        /*!
        * \brief <+description of block+>
        * \ingroup Blocs_C
        *
        */
        class BLOCS_C_API Generator_V5_c : virtual public gr::sync_block
        {
            public:
                typedef boost::shared_ptr<Generator_V5_c> sptr;

                /*!
                * \brief Return a shared_ptr to a new instance of Blocs_C::Generator_V5_c.
                *
                * To avoid accidental use of raw pointers, Blocs_C::Generator_V5_c's
                * constructor is in a private implementation
                * class. Blocs_C::Generator_V5_c::make is the public interface for
                * creating new instances.
                */
                static sptr make(
                    const int Nb_ch,
                    const std::vector<int> activeChannels,
                    const std::vector<float> occupancyRate,
                    const int preamLength,
                    const int dataLength,
                    const float digitalGain
                );
        };

    } // namespace Blocs_C
} // namespace gr

#endif /* INCLUDED_BLOCS_C_GENERATOR_V5_C_H */

