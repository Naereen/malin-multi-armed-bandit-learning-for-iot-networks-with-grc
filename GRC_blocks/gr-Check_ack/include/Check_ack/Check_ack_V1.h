/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_CHECK_ACK_CHECK_ACK_V1_H
#define INCLUDED_CHECK_ACK_CHECK_ACK_V1_H

#include <Check_ack/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace Check_ack {

    /*!
     * \brief <+description of block+>
     * \ingroup Check_ack
     *
     */
    class CHECK_ACK_API Check_ack_V1 : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<Check_ack_V1> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of Check_ack::Check_ack_V1.
       *
       * To avoid accidental use of raw pointers, Check_ack::Check_ack_V1's
       * constructor is in a private implementation
       * class. Check_ack::Check_ack_V1::make is the public interface for
       * creating new instances.
       */
      static sptr make(const size_t Nb_channels,const size_t Packet_length);
    };

  } // namespace Check_ack
} // namespace gr

#endif /* INCLUDED_CHECK_ACK_CHECK_ACK_V1_H */

