/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CHECK_ACK_CHECK_ACK_V1_IMPL_H
#define INCLUDED_CHECK_ACK_CHECK_ACK_V1_IMPL_H

#include <Check_ack/Check_ack_V1.h>

namespace gr {
  namespace Check_ack {

    class Check_ack_V1_impl : public Check_ack_V1
    {
     private:
      // Nothing to declare in this block.
	  const size_t d_Nb_channels;
	  const size_t d_Packet_length;
 	  std::vector<int> d_real_part;
	  std::vector<int> d_imag_part;
	  //float* d_real_part;
 	  //float* d_imag_part;

     public:
      Check_ack_V1_impl(const size_t Nb_channels,const size_t Packet_length);
      ~Check_ack_V1_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace Check_ack
} // namespace gr

#endif /* INCLUDED_CHECK_ACK_CHECK_ACK_V1_IMPL_H */

