/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <gnuradio/random.h>
#include "Test_ack_source_impl.h"

namespace gr {
  namespace Check_ack {

    Test_ack_source::sptr
    Test_ack_source::make(int Nb_ch, int Packet_l)
    {
      return gnuradio::get_initial_sptr
        (new Test_ack_source_impl(Nb_ch, Packet_l));
    }

    /*
     * The private constructor
     */
    Test_ack_source_impl::Test_ack_source_impl(int Nb_ch,int Packet_l)
      : gr::sync_block("Test_ack_source",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1, 1, Nb_ch* sizeof(gr_complex))),
              d_Nb_ch(Nb_ch),
              d_Packet_l(Packet_l),
              d_count(0), // Count for data generation
              d_channel_chosen(0) // Channel in which the ack is transmitted
    {
    set_output_multiple(128);
    d_rng = new gr::random(((int) time(NULL)), 0, d_Nb_ch-1);
    }

    /*
     * Our virtual destructor.
     */
    Test_ack_source_impl::~Test_ack_source_impl()
    {
        delete d_rng;
    }
    
    int Test_ack_source_impl::random_value_int()
    {
      return d_rng->ran_int();
    }

    int
    Test_ack_source_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      gr_complex *out = (gr_complex *) output_items[0];

      // Do <+signal processing+>
      // In this file, we first transmit a 1 s packet in one channel, then, we wait for 2s and the process restarts
      for(int i=0;i<noutput_items;i++)
      {
        if(d_count<d_Packet_l)
        {
                // A packet is transmitted in the chosen channel
                out[i*d_Nb_ch+d_channel_chosen] = 1+1j;
                d_count++;
        }
        else if(d_count<3*d_Packet_l-1)
        {
                 d_count++;
                 out[i*d_Nb_ch+d_channel_chosen] = 0;
        }
        else
        {
                d_count = 0;
                out[i*d_Nb_ch+d_channel_chosen] = 0;
                d_channel_chosen                = random_value_int();
                std::cout << "Prochain canal" << std::endl;
                std::cout << d_channel_chosen << std::endl;
        }
      }
      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace Check_ack */
} /* namespace gr */

