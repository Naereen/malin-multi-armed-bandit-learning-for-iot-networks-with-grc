/* -*- c++ -*- */
/*
 * Copyright 2018 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "Check_ack_V2_impl.h"
#include <gnuradio/io_signature.h>


// Turn this on by commenting (false) and uncommenting (true), for full debug output
#define DOYOUWANTODEBUG (true)
// #define DOYOUWANTODEBUG (false)

// If we receive a message which is at least 30% full but not full, we reinitialize
#define WAIT_BEFORE_REINITIALIZING_AFTER_INCOMPLETE_DATA (0.3)

// internal memory to decode binary on shifting window of char input
#define NUMBER_OF_DIFFERENT_CHAR_ACKS (5)

int max_of_int(int x, int y) {
    return x >= y ? x : y;
}

// Define the tolerance for is_close checks
#define EPSILON (0.01)

bool is_close(float x, float y) {
    return (std::abs(x - y) <= EPSILON);
}

// Map a complex input to an int output
// r_data, i_data are IGNORED in this function
int map_complex_input_to_int_output_default(
    gr_complex input,
    bool is_not_gateway
) {
    float re = std::real(input);
    float im = std::imag(input);
    if (is_close(re, +1)) {
        if (is_close(im, +1)) {
            // +1 +1j ==> 4
            return 4;
        }
        // FIXME commenting this makes the Gateway not listen to +1-1j
        if (is_not_gateway && (is_close(im, -1))) {
            // +1 -1j ==> 3
            return 3;
        }
    }
    if (is_close(re, -1)) {
        if (is_close(im, +1)) {
            // -1 +1j ==> 2
            return 2;
        }
        // FIXME commenting this makes the Gateway not listen to -1-1j
        if (is_not_gateway && (is_close(im, -1))) {
            // -1 -1j ==> 1
            return 1;
        }
    }
    return 0;  // DEFAULT
}


int map_complex_input_to_int_output_notgateway(
    gr_complex input,
    float r_data,
    float i_data
) {
    // if not is_gateway, use this one
    if (
        (is_close(std::real(input), r_data))
        &&
        (is_close(std::imag(input), i_data))
    ) {
        return map_complex_input_to_int_output_default(input, true);
    }
    return 0;  // DEFAULT
}

/*
* In this file, we count the number of elements equal to +-1+-1j. When this number
* exceeds the size of a packet (minus the tolerated error), a packet is detected.
*/
namespace gr {
    namespace Check_ack {

        Check_ack_V2::sptr
        Check_ack_V2::make(
            const int   Nb_ch,
            const std::vector<int> Active_ch,
            const int   Packet_l,
            const float Tot_err,
            const float err_block,
            const bool  is_gateway,
            const float r_data,
            const float i_data
        ) {
            return gnuradio::get_initial_sptr(
                new Check_ack_V2_impl(
                    Nb_ch,
                    Active_ch,
                    Packet_l,
                    Tot_err,
                    err_block,
                    is_gateway,
                    r_data,
                    i_data
                )
            );
        }

        /*
        * The private constructor
        */
        Check_ack_V2_impl::Check_ack_V2_impl(
            const int   Nb_ch,
            const std::vector<int> Active_ch,
            const int   Packet_l,
            const float Tot_err,
            const float err_block,
            const bool  is_gateway,
            const float r_data,
            const float i_data
        )
            : gr::sync_block(
                "Check_ack_V2",
                gr::io_signature::make(1, 1, Nb_ch * sizeof(gr_complex)),
                gr::io_signature::make(1, 1, Nb_ch * sizeof(int))
            ),
            d_Nb_ch(Nb_ch),
            d_activeChannels(Active_ch),
            d_activeChannelNum(Active_ch.size()),
            d_Packet_l(Packet_l),
            d_size_block_error(0),
            d_size_detect_packet(0),
            d_is_gateway(is_gateway),
            d_r_data(r_data),
            d_i_data(i_data),
            d_count_ok(new int[Nb_ch]),
            d_count_error(new int[Nb_ch]),
            d_counter_start(new int[Nb_ch]),
            d_count_ok_temp(new int[Nb_ch]),
            d_data_char_acks(new int[Nb_ch]),  // pour chaque canal, garde en mémoire ce qu'on doit envoyer
            d_most_seen_char_acks(new int[Nb_ch * NUMBER_OF_DIFFERENT_CHAR_ACKS]),
            d_total_seen_char_acks(new int[Nb_ch])
        {
            std::cout << std::endl << "Creating a new 'Check_ack_V2_impl.cc' block..." << std::endl;  // DEBUG
            int output_mutiple = sizeof(int) * d_Nb_ch;
            std::cout << std::endl << "With output_mutiple = " << output_mutiple << " ..." << std::endl;  // DEBUG
            set_output_multiple( output_mutiple );

            std::cout << "d_Nb_ch = " << d_Nb_ch << std::endl;
            std::cout << "d_activeChannelNum = " << d_activeChannelNum << std::endl;
            for (int index_of_channel = 0; index_of_channel < d_activeChannelNum; index_of_channel++) {
                std::cout << "    d_activeChannels[" << index_of_channel << "] = " << d_activeChannels[index_of_channel] << std::endl;  // DEBUG
            }
            std::cout << "d_Packet_l = " << d_Packet_l << std::endl;
            std::cout << "d_is_gateway = " << d_is_gateway << std::endl;
            std::cout << "d_r_data = " << d_r_data << std::endl;
            std::cout << "d_i_data = " << d_i_data << std::endl;

            d_size_detect_packet = (int) floor(d_Packet_l * (1 - Tot_err));
            std::cout << "d_size_detect_packet = " << d_size_detect_packet << std::endl;
            d_size_block_error = (int) floor(d_Packet_l * err_block);
            std::cout << "d_size_block_error = " << d_size_block_error << std::endl;

            std::cout << std::endl << std::endl << "Initializing all arrays with 0, 0, 0 and 0+0j respectively..." << std::endl;
            for (int channel = 0; channel < d_Nb_ch; channel++) {
                d_count_ok[channel]       = 0;
                d_count_error[channel]    = 0;
                d_counter_start[channel]  = 0;
                d_count_ok_temp[channel]  = 0;
                d_data_char_acks[channel] = 0;
                d_total_seen_char_acks[channel] = 0;
                for (int char_acks = 0; char_acks < NUMBER_OF_DIFFERENT_CHAR_ACKS; char_acks++) {
                    d_most_seen_char_acks[channel * NUMBER_OF_DIFFERENT_CHAR_ACKS + char_acks] = 0;
                }
            }
        }

        /*
        * Our virtual destructor.
        */
        Check_ack_V2_impl::~Check_ack_V2_impl() {}

        /*
        * Signal processing
        */
        int Check_ack_V2_impl::work(
            int noutput_items,
            gr_vector_const_void_star &input_items,
            gr_vector_void_star &output_items
        ) {
            // std::cout << "Start Check_ack_V2_impl::work()..." << std::endl;  // DEBUG

            const gr_complex *in = (const gr_complex *)input_items[0];
            int *out = (int *)output_items[0];

            int temp_char_output = 0;
            int channel = d_activeChannels[0];
            int offset = 0;

            // We count here the number of good symbols
            for (int i = 0; i < noutput_items; i++) {

                for (int index_of_channel = 0; index_of_channel < d_activeChannelNum; index_of_channel++) {
                    channel = d_activeChannels[index_of_channel];
                    offset = (i * d_Nb_ch + channel);

                    // on est prudent, remise a zero de la sortie
                    out[offset] = 0;

                    // We count if the received message/ack is close enough to one of the 4 complex +-1+-1j
                    if ( d_is_gateway ) {
                        // temp_char_output = map_complex_input_to_int_output_default(in[offset], false);
                        temp_char_output = map_complex_input_to_int_output_default(in[offset], (d_data_char_acks[channel] != 0));
                        // if ((temp_char_output != 0) && (temp_char_output != d_data_char_acks[channel])) {
                        //     printf("->%i", temp_char_output);  // DEBUG
                        // }
                        // if d_data_char_acks[channel] is 0, maybe this is the preamble
                        // so the gateway should not listen to +-1-1j (conf)
                        // but if d_data_char_acks[channel] != 0, we already saw the preamble
                    } else {
                        // XXX here we take the conjugate
                        // to tell the object to listen to the conjugate of its output message
                        // as the gateway reply with a conjugate acknowledgement
                        temp_char_output = map_complex_input_to_int_output_notgateway(in[offset], d_r_data, -d_i_data);
                    }

                    if (
                        (temp_char_output > 0)
                        &&
                        (d_data_char_acks[channel] == 0)
                    ) {
                        // d_most_seen_char_acks[channel * NUMBER_OF_DIFFERENT_CHAR_ACKS + temp_char_output] += 1;
                        d_data_char_acks[channel] = temp_char_output;
                    } else
                    if (
                        (temp_char_output > 0)
                        &&
                        (d_data_char_acks[channel] == temp_char_output)
                    ) {
                        // d_most_seen_char_acks[channel * NUMBER_OF_DIFFERENT_CHAR_ACKS + temp_char_output] += 1;
                        d_data_char_acks[channel] = temp_char_output;

                        if (d_count_ok[channel] < d_size_detect_packet) {
                            d_count_ok[channel] += 1;
                        } else {
                            d_count_ok[channel] = 0;
                            out[offset] = d_data_char_acks[channel];
                            // now that this symbol was produced, no need to produce it until next time
                            d_data_char_acks[channel] = 0;
                            if (DOYOUWANTODEBUG) {  // DEBUG
                                std::cout << std::endl << std::endl << "---------------------------------------------------------"  << std::endl << "New detection in channel #" << index_of_channel << " (channel " << channel << "), input data was = " << in[offset] << " so output will be = " << (int) temp_char_output << " ..." << std::endl;

                                if( not(d_is_gateway )) {
                                    std::cout << "Acknowledgment well received on channel #" << index_of_channel << " (channel " << channel << std::endl;
                                }
                            }
                        }
                    } else if (d_counter_start[channel] == 0) {
                        d_counter_start[channel] += 1;
                    }

                    // Count the number of good symbols in the next block of d_size_block_error symbols
                    if (d_counter_start[channel] == 1) {
                        d_count_error[channel] += 1;

                        if (d_count_ok_temp[channel] >= (WAIT_BEFORE_REINITIALIZING_AFTER_INCOMPLETE_DATA * d_size_block_error)) {
                            d_count_ok_temp[channel]  = 0;
                            d_count_error[channel]    = 0;
                            d_counter_start[channel]  = 0;
                            d_data_char_acks[channel] = 0;
                        } else {
                            if ( temp_char_output > 0 ) {
                                d_count_ok_temp[channel] += 1;
                            } else {
                                d_data_char_acks[channel] = 0;
                            }
                        }

                        if (d_count_error[channel] >= d_size_block_error) {
                            d_count_ok_temp[channel]  = 0;
                            d_count_error[channel]    = 0;
                            d_counter_start[channel]  = 0;
                            d_count_ok[channel]       = 0;
                            d_data_char_acks[channel] = 0;
                        }
                    }
                }
            }

            // if (d_is_gateway) {
            //     for (int channel = 0; channel < d_Nb_ch; channel++) {
            //         for (int char_acks = 1; char_acks < NUMBER_OF_DIFFERENT_CHAR_ACKS; char_acks++) {
            //             d_total_seen_char_acks[channel] = max_of_int(d_total_seen_char_acks[channel], d_most_seen_char_acks[channel * NUMBER_OF_DIFFERENT_CHAR_ACKS + char_acks]);
            //         }
            //         if (d_total_seen_char_acks[channel] > 900) {
            //             for (int char_acks = 0; char_acks < NUMBER_OF_DIFFERENT_CHAR_ACKS; char_acks++) {
            //                 if (d_most_seen_char_acks[channel * NUMBER_OF_DIFFERENT_CHAR_ACKS + char_acks] > 0) {
            //                     printf("  For channel %i, input char %i was seen %i times...\n", channel, char_acks, d_most_seen_char_acks[channel * NUMBER_OF_DIFFERENT_CHAR_ACKS + char_acks]);
            //                 }
            //                 d_most_seen_char_acks[channel * NUMBER_OF_DIFFERENT_CHAR_ACKS + char_acks] = 0;
            //             }
            //             d_total_seen_char_acks[channel] = 0;
            //         }
            //     }
            // }

            // Tell runtime system how many output items we produced.
            return noutput_items;
        }

    } /* namespace Check_ack */
} /* namespace gr */
