# MALIN: *M*ulti-*A*rm bandit *L*earning for *Internet* of things *N*etworks with GRC
> SCEE / GNU Radio Companion.

This folder contains the custom blocks written for our [GNU Radio](http://gnuradio.org/) demo.

All our custom blocks are written in C++.

- TODO: use only one folder for all the 5 blocks!

## How to build
```bash
$ make
```
And that should be enough! It takes about 3 minutes (the first time).

## What is needed
GNU Radio and GNU Radio development files. See [official installation instruction](https://wiki.gnuradio.org/index.php/InstallingGR) for more details.
You need `gcc` and `python` (2).

## Copyright
Copyright (C) 2017-2018  Rémi Bonnefoi, Lilian Besson

GPLv3 Licensed ([file LICENSE](../LICENSE)).
