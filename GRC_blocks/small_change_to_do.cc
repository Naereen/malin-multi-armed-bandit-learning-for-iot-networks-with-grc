

// FIXME experimental!
#include "death_handler.cc"
#include "death_handler.h"
Debug::DeathHandler dh;


// FIXME experimental! https://stackoverflow.com/a/77336/5889533
#include <csignal>
#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
void handler_signal_code(int error_signal_code) {
    void *array_of_addresses[20];
    size_t size_of_backtrace;
    // get void*'s for all entries on the stack
    size_of_backtrace = backtrace(array_of_addresses, 20);
    // print out all the frames to stderr
    fprintf(stderr, "Error: signal %d:\n", error_signal_code);
    backtrace_symbols_fd(array_of_addresses, size_of_backtrace, STDERR_FILENO);
    exit(1);
}
signal(SIGSEGV, handler_signal_code);   // install our handler_signal_code
