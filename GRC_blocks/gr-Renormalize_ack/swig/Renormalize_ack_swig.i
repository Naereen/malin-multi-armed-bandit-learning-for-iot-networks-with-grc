/* -*- c++ -*- */

#define RENORMALIZE_ACK_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "Renormalize_ack_swig_doc.i"

%{
#include "Renormalize_ack/Renormalize_ack_v2.h"
#include "Renormalize_ack/Renormalize_ack_v3.h"
%}

%include "Renormalize_ack/Renormalize_ack_v2.h"
GR_SWIG_BLOCK_MAGIC2(Renormalize_ack, Renormalize_ack_v2);
%include "Renormalize_ack/Renormalize_ack_v3.h"
GR_SWIG_BLOCK_MAGIC2(Renormalize_ack, Renormalize_ack_v3);
