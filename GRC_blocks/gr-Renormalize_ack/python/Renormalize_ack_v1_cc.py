#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from __future__ import print_function

import numpy as np
from gnuradio import gr

DEBUG = True
DEBUG = False


def normsq(z):
    return z.real**2 + z.imag**2

def norm(z):
    return np.sqrt(z.real**2 + z.imag**2)


b_00, b_01, b_10, b_11 = -1-1j, -1+1j, 1-1j, 1+1j


normed_b_00, normed_b_01, normed_b_10, normed_b_11 = (-1-1j)/np.sqrt(2), (-1+1j)/np.sqrt(2), (1-1j)/np.sqrt(2), (1+1j)/np.sqrt(2)

assert np.isclose(norm(normed_b_00), 1), "Erreur : le calcul de normed_b_00 a échoué (il doit être de norme = 1)..."  # DEBUG
assert np.isclose(norm(normed_b_01), 1), "Erreur : le calcul de normed_b_01 a échoué (il doit être de norme = 1)..."  # DEBUG
assert np.isclose(norm(normed_b_10), 1), "Erreur : le calcul de normed_b_10 a échoué (il doit être de norme = 1)..."  # DEBUG
assert np.isclose(norm(normed_b_11), 1), "Erreur : le calcul de normed_b_11 a échoué (il doit être de norme = 1)..."  # DEBUG


class Renormalize_ack_v1_cc(gr.sync_block):
    """
    Le block Renormalize_ack_v1_cc doit :

    - recevoir en entree un vecteur d'entier de taille Nb_ch, et sur les canaux actifs (la liste Active_ch), verifier quel canal devient actif (passer de 0 a un complexe non nul).
    - emettre en sortie un vecteur de complexe de taille Nb_ch, pour chaque canal avec 1+1j s'il contenait un complexe non nul (après Preamb_l instants, et pendant Data_l instants), et 0 sinon.
    """
    def __init__(self, Nb_ch, Active_ch, Preamb_l, Data_l, Threshold_norm):
        gr.sync_block.__init__(self,
            name="Renormalize_ack_v1_cc",
            in_sig=[(np.complex64, Nb_ch)],
            out_sig=[(np.complex64, Nb_ch)])

        # Quelques tests
        assert len(set(Active_ch)) == len(Active_ch), "Erreur : Active_ch contient deux fois le meme canal."
        assert tuple(sorted(Active_ch)) == tuple(Active_ch), "Erreur : Active_ch n'est pas trie dans l'ordre croissant."
        assert set(Active_ch) <= set(list(range(Nb_ch))), "Erreur : Active_ch contient des elements qui ne sont pas dans [0,...,Nb_ch]."

        # fixed attributes
        self.Nb_ch            = Nb_ch     #: int, nombre de canaux
        self.Active_ch        = Active_ch #: vecteur de taille int, contenant les canaux actifs
        self.Preamb_l         = Preamb_l  #: taille du preambule, ie, nombre d'instants a attendre avant de transformer le message
        self.Data_l           = Data_l        #: taille de l'acknowledgment
        self.Threshold_norm           = Threshold_norm        #: seuil sur la norme du complexe avant de le compter comme non nul

        # mémoires
        self.current_data_count = {
            canal: 0
            for canal in self.Active_ch
        }  #: compte le nb de data qu'on a vu depuis le début d'un message recu
        self.in_preamble = {
            canal: False
            for canal in self.Active_ch
        }  #: est-on dans un préambule ?
        self.in_message = {
            canal: False
            for canal in self.Active_ch
        }  #: est-on dans un message ?
        self.current_preamble_memory = {
            canal: []
            for canal in self.Active_ch
        }  #: mémoire des Preamb_l data du préambule
        self.multiplier = {
            canal: 1
            for canal in self.Active_ch
        }  #: moyenne des entrées du préambule, calculée une seule fois

    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]
        one_data = 0

        for canal in self.Active_ch:
            # on recoit plein de data (par bloc)
            # 1. si c'est nul, on a rien,
            # 2. si c'était nul et c'est non nul, on a un début de préambule (on commence a compter),
            # 3. si c'était non nul et c'est toujours non nul, on est dans un préambule ou un message (selon notre compte)
            # 4. si c'était non nul et c'est nul, on sors du message
            for i in range(len(in0)):
                one_data = in0[i, canal]
                out[i, canal] = one_data

                if norm(in0[i, canal]) >= self.Threshold_norm:
                    # on détecte un signal ! on compte un en plus dans la continuité des précédents
                    self.current_data_count[canal] += 1
                    # si on avait rien, on a un préambule
                    if not self.in_preamble[canal] and not self.in_message[canal]:
                        if DEBUG: print("Début préambule dans canal", canal, "avec", self.current_data_count[canal], "données vues dans ce canal...")  # DEBUG
                        self.in_preamble[canal] = True

                    if self.in_preamble[canal]:
                        self.current_preamble_memory[canal].append(one_data)
                        self.multiplier[canal] = normed_b_11 * one_data.conjugate() / normsq(one_data)
                        if DEBUG: print("Ajout signal courant,", one_data, "au tampon du préambule... multiplieur =", self.multiplier[canal], "pour le tampon...")  # DEBUG

                    if DEBUG:
                        assert norm(self.multiplier[canal]) >= self.Threshold_norm, "Erreur : le multiplieur = {:.3g} a une norme trop petite !".format(self.multiplier[canal])  # DEBUG
                        print("On tourne la sortie complexe #{}/{} depuis {} avec le multiplieur {}...".format(i, len(in0), one_data, self.multiplier[canal]))  # DEBUG
                        assert np.isclose(normsq(out[i, canal]), normsq(one_data)), "Erreur : norme de l'entrée = {:.3g} a changé à la sortie (nouvelle norme carrée = {:.3g})...".format(normsq(one_data), normsq(out[i, canal]))  # DEBUG

                elif self.current_data_count[canal] > 0:
                    # on détecte plus de signal !
                    if DEBUG: print("On ne détecte plus de signal dans le canal", canal, "alors qu'on avait vu", self.current_data_count[canal], "signaux consécutifs...")  # DEBUG
                    self.multiplier[canal] = 1
                    self.current_data_count[canal] = 0
                    # si on avait un préambule ou un message, on l'a plu !
                    self.in_preamble[canal] = False
                    self.current_preamble_memory[canal] = []  # on efface le tampon du préambule
                    self.in_message[canal] = False

                # si on avait assez et qu'on a vu tout le préambule, on a un message
                if self.in_preamble[canal] and self.current_data_count[canal] >= self.Preamb_l:
                    if DEBUG: print("Fin préambule dans canal", canal, ", début message...")  # DEBUG
                    assert self.in_preamble[canal], "Erreur : on a vu la fin d'un préambule alors qu'on était pas dedans."
                    self.in_preamble[canal] = False
                    self.in_message[canal] = True
                    assert len(self.current_preamble_memory[canal]) >= self.Preamb_l, "Erreur : on a utilisé la mémoire du tampon de ce canal, mais elle n'avait que {} données au lieu de {}...".format(len(self.current_preamble_memory[canal]), self.Preamb_l)
                    mean_data = np.mean(self.current_preamble_memory[canal])  # on calcule ca une seule fois
                    self.multiplier[canal] = normed_b_11 * mean_data.conjugate() / normsq(mean_data)
                    if DEBUG: print("Avec un tampon de", self.Preamb_l, "données du préambule, on a un préambule moyen =", mean_data, "et un multiplieur =", self.multiplier[canal])  # DEBUG

                # si on avait assez et qu'on a vu tout le préambule + le message, on a tout vu
                elif self.in_message[canal] and self.current_data_count[canal] >= self.Preamb_l + self.Data_l:
                    if DEBUG: print("Fin message dans canal", canal, ", début attente...")  # DEBUG
                    assert not self.in_preamble[canal], "Erreur : on a vu la fin d'un message alors qu'on était dans un préambule."
                    assert self.in_message[canal], "Erreur : on a vu la fin d'un message alors qu'on était pas dedans."
                    self.in_message[canal] = False

                # si on est pas dans le préambule on calcul le multiplieur
                if self.multiplier[canal] != 1:
                    out[i, canal] *= self.multiplier[canal]
                    out[i, canal] /= norm(out[i, canal])
                    if DEBUG: print("On normalise la sortie complexe #{}/{} depuis {} vers {}...".format(i, len(in0), one_data, out[i, canal]))  # DEBUG
                    assert np.isclose(norm(out[i, canal]), 1), "Erreur : la sortie doit etre projetée sur le cercle unité (ie norme = 1) mais ici elle a une norme = {}.".format(norm(out[i, canal]))

        return len(output_items[0])

    def old_work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]
        one_data = 0

        for canal in self.Active_ch:
            multiplier = 1

            for i in range(len(in0)):
                one_data = in0[i, canal]
                out[i, canal] = one_data

                if norm(in0[i, canal]) >= self.Threshold_norm:

                    multiplier = normed_b_11 * one_data.conjugate() / normsq(one_data)

                    if DEBUG: assert norm(multiplier) >= self.Threshold_norm, "Erreur : le multiplieur = {:.3g} a une norme trop petite !".format(multiplier)  # DEBUG
                    if DEBUG: print("On tourne la sortie complexe #{}/{} depuis {} avec le multiplieur {}...".format(i, len(in0), one_data, multiplier))  # DEBUG
                    if DEBUG: assert np.isclose(normsq(out[i, canal]), normsq(one_data)), "Erreur : norme de l'entrée = {:.3g} a changé à la sortie (nouvelle norme carrée = {:.3g})...".format(normsq(one_data), normsq(out[i, canal]))  # DEBUG

                    out[i, canal] *= multiplier
                    if DEBUG: print("On normalise la sortie complexe #{}/{} depuis {} vers {}...".format(i, len(in0), one_data, out[i, canal]))  # DEBUG

                    print("Signal courant,", one_data, "... Multiplier =", multiplier, "sortie =", out[i, canal])  # DEBUG

        return len(output_items[0])

