/* -*- c++ -*- */
/*
 * Copyright 2018 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "Renormalize_ack_v2_cc.h"
#include <gnuradio/io_signature.h>
#include <complex>    // std::complex

#include <assert.h>

// Turn this on by commenting (false) and uncommenting (true), for full debug output
// #define DOYOUWANTODEBUG (true)
#define DOYOUWANTODEBUG (false)

// Turn this on by commenting (false) and uncommenting (true), to force norm one of output or use +-1+-1j
// #define NORM_ONE_NOT_PM1J (true)
#define NORM_ONE_NOT_PM1J (false)

// If we receive a message which is at least 75% full but not full, a log line is printed
#define WAIT_BEFORE_COMPLAINING_OF_INCOMPLETE_DATA (0.75)

// Fraction of good data counted as a preamble, and fraction of bad data before message is counted as interrupted. Experimental!
#define RATE_COUNT_GOOD_DATA_PREAMBLE (0.5)
#define RATE_COUNT_ZERO_DATA_INTERUPT (0.5)

// Threshold on norms of complex numbers before defining as zero
float THRESHOLD_ON_NORM = 0.001;

// Some numerical constants
float SQRT_2            = 1.4142135623;  // sqrt(2)
float ONE_BY_SQRT_2     = 0.7071067811;  // 1/sqrt(2) = sqrt(2)/2

// Complex constants
gr_complex b_00        = -1-1j;
gr_complex b_01        = -1+1j;
gr_complex b_10        = +1-1j;
gr_complex b_11        = +1+1j;
gr_complex normed_b_00 = -0.7071067811 - 0.7071067811j;
gr_complex normed_b_01 = -0.7071067811 + 0.7071067811j;
gr_complex normed_b_10 =  0.7071067811 - 0.7071067811j;
gr_complex normed_b_11 =  0.7071067811 + 0.7071067811j;

// WHAT THE F*CK IS C++
// THERE IS NO POWER FUNCTION IN THE COMMON SYNTAX THIS IS STUPID!!
float square(float x) {
    return x * x;
}

float normsq(gr_complex z) {
    return square(std::norm(z));
}

bool is_complex_one(gr_complex z) {
    return ((std::real(z) == 1) && (std::imag(z) == 0));
}

int min_of_int(int x, int y) {
    if (x <= y) {
        return x;
    } else {
        return y;
    }
}


/*
    Le block Renormalize_ack_v2_cc doit :

    - recevoir en entree un vecteur de complex de taille Nb_ch, et sur les canaux actifs (la liste Active_ch), verifier quel channel devient actif (passer de 0 a un complexe non nul).
    - emettre en sortie un vecteur de complexe de taille Nb_ch, pour chaque channel avec un changement de phase pour mettre les 1000 premiers symboles d'un message à 1+j et la suite soit tournée (changement de phase) comme il faut.
*/
namespace gr {
    namespace Renormalize_ack {

        Renormalize_ack_v2::sptr
        Renormalize_ack_v2::make(
            const int Nb_ch,
            const std::vector<int> Active_ch,
            const int Preamb_l,
            const int Data_l,
            const float Threshold_norm
        )
        {
        return gnuradio::get_initial_sptr(
            new Renormalize_ack_v2_cc(
                    Nb_ch,
                    Active_ch,
                    Preamb_l,
                    Data_l,
                    Threshold_norm
                )
            );
        }

        /*
        * The private constructor
        */
        Renormalize_ack_v2_cc::Renormalize_ack_v2_cc(
                const int Nb_ch,
                const std::vector<int> Active_ch,
                const int Preamb_l,
                const int Data_l,
                const float Threshold_norm
            ) :
            gr::sync_block(
                "Renormalize_ack_v2",
                gr::io_signature::make(1, 1, Nb_ch * sizeof(gr_complex)),
                gr::io_signature::make(1, 1, Nb_ch * sizeof(gr_complex))
            ),
            d_Nb_ch(Nb_ch),
            d_activeChannels(Active_ch),
            d_activeChannelNum(Active_ch.size()),
            d_Preamb_l(Preamb_l),
            d_Data_l(Data_l),
            d_Threshold_norm(Threshold_norm),
            // attributes
            d_current_data_count(new int[Nb_ch]),  // compte le nb de data qu'on a vu depuis le début d'un message recu
            d_current_zero_data_count(new int[Nb_ch]),  // compte le nb de data zero qu'on a vu pendant le message recu
            d_in_preamble(new bool[Nb_ch]),  // est-on dans un préambule ?
            d_in_message(new bool[Nb_ch]),  // est-on dans un message ?
            d_current_preamble_memory_offset(new int[Nb_ch]),  // indice de fin de la mémoire des Preamb_l data du préambule
            d_current_preamble_memory(new gr_complex[(Nb_ch * Preamb_l * 1)]),  // mémoire des Preamb_l data du préambule
            // d_current_preamble_mean(new gr_complex[Nb_ch]),  // mémoire des moyennes des Preamb_l data du préambule
            d_multiplier(new gr_complex[Nb_ch])  // moyenne des entrées du préambule, calculée une seule fois
        {
            // set_output_multiple(128);  // FIXME

            std::cout << std::endl << "Creating a new 'Renormalize_ack_v2_cc.cc' block..." << std::endl;  // DEBUG
            int output_mutiple = sizeof(gr_complex) * d_Nb_ch;
            std::cout << std::endl << "With output_mutiple = " << output_mutiple << " ..." << std::endl;  // DEBUG
            set_output_multiple( output_mutiple );

            // DEBUG show input values of parameters
            std::cout << "d_Nb_ch: " << d_Nb_ch << std::endl;  // DEBUG
            std::cout << "d_activeChannelNum: " << d_activeChannelNum << std::endl;  // DEBUG
            for (int index_of_channel = 0; index_of_channel < d_activeChannelNum; index_of_channel++) {
                std::cout << "    d_activeChannels[" << index_of_channel << "] = " << d_activeChannels[index_of_channel] << std::endl;  // DEBUG
            }
            std::cout << "d_Preamb_l: " << d_Preamb_l << std::endl;  // DEBUG
            std::cout << "d_Data_l: " << d_Data_l << std::endl;  // DEBUG
            std::cout << "d_Threshold_norm: " << d_Threshold_norm << std::endl;  // DEBUG

            std::cout << std::endl << std::endl << "Initializing all arrays with 0, false, false, 0, 0+0j and 0+0j respectively..." << std::endl;
            // WARNING be careful and put every array to default values
            for (int channel = 0; channel < d_Nb_ch; channel++) {
                d_current_data_count[channel]             = 0;
                d_current_zero_data_count[channel]        = 0;
                d_in_preamble[channel]                    = false;
                d_in_message[channel]                     = false;
                d_current_preamble_memory_offset[channel] = 0;
                d_multiplier[channel]                     = 0+0j;
                for (int t = 0; t < d_Preamb_l; t++) {
                    d_current_preamble_memory[t * d_Nb_ch + channel] = 0+0j;
                    // d_current_preamble_mean[t * d_Nb_ch + channel]   = 0+0j;
                }
            }
        }

        /*
        * Our virtual destructor.
        */
        Renormalize_ack_v2_cc::~Renormalize_ack_v2_cc() {}

        /*
        * Signal processing
        */
        int Renormalize_ack_v2_cc::work(
            int noutput_items,
            gr_vector_const_void_star &input_items,
            gr_vector_void_star &output_items
        ) {
            //std::cout << "Start Renormalize_ack_v2_cc::work()..." << std::endl;  // DEBUG

            const gr_complex *in = (const gr_complex *)input_items[0];
            gr_complex *out = (gr_complex *)output_items[0];

            int channel = d_activeChannels[0];
            int offset  = channel;

            gr_complex one_data  = 0+0j;

            for (int index_of_channel = 0; index_of_channel < d_activeChannelNum; index_of_channel++) {
                channel = d_activeChannels[index_of_channel];

                // on recoit plein de data (par bloc)
                // 1. si c'est nul, on a rien,
                // 2. si c'était nul et c'est non nul, on a un début de préambule (on commence a compter),
                // 3. si c'était non nul et c'est toujours non nul, on est dans un préambule ou un message (selon notre compte)
                // 4. si c'était non nul et c'est nul, on sort du message

                for (int i = 0; i < noutput_items; i++) {
                    offset = (i * d_Nb_ch) + channel;

                    one_data = in[offset];
                    // on copie l'entrée, qu'on va peut-être changer plus tard
                    out[offset] = one_data;

                    // on détecte un signal ! on compte un en plus dans la continuité des précédents
                    if ( std::norm(one_data) > d_Threshold_norm ) {
                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "Is the segfault happening in this part #1/6 ?" << std::endl;
                        }
                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << std::endl << "We detected a signal in channel " << channel << " of norm = " << std::norm(one_data) << std::endl;  // DEBUG
                        }
                        // on détecte un signal ! on compte un en plus dans la continuité des précédents
                        d_current_data_count[channel] += 1;
                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "d_current_data_count[channel] = " << d_current_data_count[channel] << " for channel = " << channel << std::endl;  // DEBUG
                        }

                        // DEBUG remove this assert when done debugging
                        assert(d_current_data_count[channel] <= (2 + (d_Preamb_l + d_Data_l)));

                        // si on avait rien, on a un préambule
                        if (
                            ( d_in_preamble[channel] == false )
                            &&
                            ( d_in_message[channel]  == false )
                        ) {
                            std::cout << std::endl << "Start of preamble in channel " << channel << "..." << std::endl;  // DEBUG
                            d_in_preamble[channel] = true;
                            d_in_message[channel]  = false;  // just be sure
                        }

                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "No segfault, not in this part #1/6..." << std::endl;
                        }

                        if ( d_in_preamble[channel] ) {
                            if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                                std::cout << "Is the segfault happening in this part #2/6 ?" << std::endl;
                            }

                            // DEBUG remove this assert when done debugging
                            assert( (0 <= d_current_preamble_memory_offset[channel]) && (d_current_preamble_memory_offset[channel] < d_Preamb_l) );

                            if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                                std::cout << std::endl << "Adding current data, " << one_data << " to the preamble memory... at location =" << d_current_preamble_memory_offset[channel] * d_Nb_ch + channel << ", with channel=" << channel << " and d_current_preamble_memory_offset[channel]=" << d_current_preamble_memory_offset[channel] << " and d_Nb_ch=" << d_Nb_ch << "..." << std::endl;  // DEBUG
                            }

                            // XXX The shifting window is HERE
                            // d_current_preamble_mean[d_current_preamble_memory_offset[channel] * d_Nb_ch + channel] += (one_data ) / d_Preamb_l;
                            d_current_preamble_memory[d_current_preamble_memory_offset[channel] * d_Nb_ch + channel] = one_data;

                            // FIXED this was the issue of the segfault, unbounded access!
                            // d_current_preamble_memory_offset[channel] += 1;
                            d_current_preamble_memory_offset[channel] = min_of_int(d_current_preamble_memory_offset[channel] + 1, d_Preamb_l);
                            // d_current_preamble_memory_offset[channel] = (d_current_preamble_memory_offset[channel] + 1) % d_Preamb_l;

                            if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                                std::cout << "No segfault, not in this part #2/6..." << std::endl;
                            }
                        }
                    } else {
                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "Is the segfault happening in this part #3/6 ?" << std::endl;
                        }

                        // one more data with small norm (zero data)
                        d_current_zero_data_count[channel] += 1;
                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "d_current_zero_data_count[channel] = " << d_current_zero_data_count[channel] << " for channel = " << channel << std::endl;  // DEBUG
                        }
                        // DEBUG remove this assert when done debugging
                        assert(d_current_zero_data_count[channel] <= (1 + (d_Preamb_l + d_Data_l)));

                        // XXX New! If we have seen enough zero data, we reset count of non zero data
                        if ( d_current_zero_data_count[channel] > (int) floor(RATE_COUNT_ZERO_DATA_INTERUPT * d_Preamb_l) ) {

                            if (DOYOUWANTODEBUG) {
                                if (d_current_data_count[channel] >= (WAIT_BEFORE_COMPLAINING_OF_INCOMPLETE_DATA * d_Preamb_l)) {
                                    std::cout << std::endl << "We no longer detect signal in channel " << channel << " after seeing " << WAIT_BEFORE_COMPLAINING_OF_INCOMPLETE_DATA << "% of the input, ie, " << d_current_data_count[channel] << " consecutive data..." << std::endl;  // DEBUG
                                }
                            }

                            // on détecte plus de signal !
                            d_current_data_count[channel]             = 0;
                            // on détecte plus de signal !
                            d_current_zero_data_count[channel]        = 0;
                            // si on avait un préambule ou un message, on l'a plu !
                            d_in_preamble[channel]                    = false;
                            d_in_message[channel]                     = false;
                            d_current_preamble_memory_offset[channel] = 0;  // on efface le tampon du préambule
                            // pas besoin de faire une boucle pour effacer vraiment le tampon, on reinitialise juste l'offset
                        }

                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "No segfault, not in this part #3/6..." << std::endl;
                        }
                    }

                    // XXX New! If we have seen enough non zero data, we reset count of zero data
                    if ( d_current_data_count[channel] >= (int) floor(RATE_COUNT_GOOD_DATA_PREAMBLE * d_Preamb_l) ) {
                        d_current_zero_data_count[channel] = 0;
                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "Setting d_current_zero_data_count[channel] = " << d_current_zero_data_count[channel] << " for channel = " << channel << std::endl;  // DEBUG
                        }
                    }

                    // si on avait assez et qu'on a vu tout le préambule, on a un message
                    if (
                        ( d_in_preamble[channel] )
                        &&
                        ( d_current_data_count[channel] >= d_Preamb_l )
                    ) {
                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "Is the segfault happening in this part #4/6 ?" << std::endl;
                        }

                        std::cout << std::endl << "End of preamble in channel " << channel << ", starting to receive message..." << std::endl;  // DEBUG
                        d_in_preamble[channel] = false;
                        d_in_message[channel]  = true;

                        // on calcule ca une seule fois en une boucle simple
                        gr_complex mean_data = 0+0j;
                        int length_of_the_current_preamble_memory = min_of_int(d_Preamb_l, d_current_preamble_memory_offset[channel]);
                        std::cout << "length_of_the_current_preamble_memory = " << length_of_the_current_preamble_memory << " for channel = " << channel << std::endl;  // DEBUG

                        if ( length_of_the_current_preamble_memory > 0 ) {
                            if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                                std::cout << std::endl << "Starting to compute the mean of preamble memory, of length = " << length_of_the_current_preamble_memory << std::endl;
                            }
                            for (int t = 0; t < length_of_the_current_preamble_memory; t++) {
                                mean_data += d_current_preamble_memory[t * d_Nb_ch + channel];

                                if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                                    std::cout << "Using this data " << d_current_preamble_memory[t * d_Nb_ch + channel] << " at position " << (t * d_Nb_ch + channel) << " (for t=" << t << " * d_Nb_ch=" << d_Nb_ch << " + channel=" << channel << ")... mean_data=" << mean_data << " ..." << std::endl;
                                }
                            }
                            if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                                std::cout << "Current mean_data =" << mean_data << " and we divide it..." << std::endl;
                            }
                            mean_data /= length_of_the_current_preamble_memory;
                            if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                                std::cout << "Final mean_data =" << mean_data << "..." << std::endl;
                            }
                        }

                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "No segfault, not in this first part #4/6... Second part maybe?" << std::endl;
                        }

                        // si on est pas dans le préambule on calcule le multiplieur
                        if (std::norm(mean_data) > THRESHOLD_ON_NORM) {
                            d_multiplier[channel] = std::conj(mean_data) / std::norm(mean_data);
                            // on force la sortie a avoir une norme = 1 ?
                            d_multiplier[channel] *= (normed_b_11);
                            std::cout << std::endl << "With a preamble of " << length_of_the_current_preamble_memory << " data, we got a mean preamble = " << mean_data << " and a multiplier = " << d_multiplier[channel] << " (of norm =" << std::norm(d_multiplier[channel]) << ")..." << std::endl;  // DEBUG
                        }

                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "No segfault, not in this second part #4/6..." << std::endl;
                        }
                    }
                    else {
                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "Is the segfault happening in this part #5/6 ?" << std::endl;
                        }

                        if (
                            // si on avait assez et qu'on a vu tout le préambule + le message, on a tout vu
                            ( d_in_message[channel] )
                            &&
                            ( d_current_data_count[channel] >= (d_Preamb_l + d_Data_l) )
                        ) {
                            if (DOYOUWANTODEBUG) {
                                std::cout << std::endl << "End of message in channel " << channel << ", starting to wait..." << std::endl;  // DEBUG
                            }
                            d_in_message[channel]         = false;
                            d_current_data_count[channel] = 0;
                            // we can reset the multiplier!
                            d_multiplier[channel]         = 0+0j;
                        }

                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "No segfault, not in this part #5/6..." << std::endl;
                        }
                    }

                    // Enfin, on multiplie la sortie par ce coefficient
                    // if the output is not too close to zero
                    if (
                        ( std::norm(one_data) > THRESHOLD_ON_NORM )
                        &&
                        ( std::norm(d_multiplier[channel]) > THRESHOLD_ON_NORM )
                    ) {
                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "Is the segfault happening in this part #6/6 ?" << std::endl;
                        }

                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "i =" << i << " and index_of_channel =" << index_of_channel << " and channel =" << channel << " gives offset = i * d_Nb_ch + channel = " << offset << " and out[offset] = " << out[offset] << " and d_multiplier[channel] =" << d_multiplier[channel] << std::endl;
                        }

                        out[offset] *= d_multiplier[channel];

                        // // FIXME est-ce qu'on force la sortie a avoir une norme = 1 ?
                        // if (NORM_ONE_NOT_PM1J) {
                        //     out[offset] /= std::norm(out[offset]);  // normalize!
                        //     // if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                        //     //     std::cout << std::endl << "We normalise the complex output # " << i << " / " << noutput_items << " from " << one_data << " to " << out[offset] << " with multiplier " << d_multiplier[channel] << " ... " << std::endl;  // DEBUG
                        //     // }
                        // } else {
                        //     out[offset] *= SQRT_2 / std::norm(out[offset]);  // normalize and project back to the circle of +-1+-1j
                        //     // if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                        //     //     std::cout << std::endl << "We project the complex output on +-1+-1j # " << i << " / " << noutput_items << " from " << one_data << " to " << out[offset] << " with multiplier " << d_multiplier[channel] << " ... " << std::endl;  // DEBUG
                        //     // }
                        // }

                        if ( (DOYOUWANTODEBUG) && (i == 0) ) {
                            std::cout << "No segfault, not in this part #6/6..." << std::endl;
                        }
                    }
                }
            }

            // Tell runtime system how many output items we produced.
            return noutput_items;
        }

    } /* namespace Renormalize_ack */
} /* namespace gr */
