/* -*- c++ -*- */
/*
 * Copyright 2018 Remi Bonnefoi and Lilian Besson, CentraleSupelec campus of Rennes
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "Renormalize_ack_v3_impl.h"
#include <complex>    // std::complex

// Complex constants on the complex unit circle
gr_complex complex_unitary_symbol_00 =  1.0 + 1.0j;
gr_complex complex_unitary_symbol_01 = -1.0 + 1.0j;
gr_complex complex_unitary_symbol_10 =  1.0 - 1.0j;
gr_complex complex_unitary_symbol_11 = -1.0 - 1.0j;


namespace gr {
    namespace Renormalize_ack {

        Renormalize_ack_v3::sptr
        Renormalize_ack_v3::make(
            const float Threshold,
            const int Nb_ch,
            const std::vector<int> Active_ch,
            const int Length_sum)
        {
        return gnuradio::get_initial_sptr
            (new Renormalize_ack_v3_impl(
                Threshold,
                Nb_ch,
                Active_ch,
                Length_sum
                )
            );
        }

        /*
        * The private constructor
        */
        Renormalize_ack_v3_impl::Renormalize_ack_v3_impl(
            const float Threshold,
            const int Nb_ch,
            const std::vector<int> Active_ch,
            const int Length_sum
        )
        : gr::sync_block("Renormalize_ack_v3",
            gr::io_signature::make(1, 1, Nb_ch * sizeof(gr_complex)),
            gr::io_signature::make(1, 1, Nb_ch * sizeof(gr_complex))
        ),
            d_Threshold(Threshold),
            d_Nb_ch(Nb_ch),
            d_Active_ch(Active_ch),
            d_Length_sum(Length_sum),
            d_count(0), // Count the number of data in the sum
            d_sum(new gr_complex[Nb_ch]), // Sum of the complex in the channel
            d_multiplier(new gr_complex[Nb_ch]), // Value of the multiplier in the channel
            d_state_ch(new int[Nb_ch]) // State of the channels, is 0 for no data, 1 start the preamble, 2 in a packet
        {

            std::cout << std::endl << "Creating a new 'Renormalize_ack_v3_impl.cc' block..." << std::endl;  // DEBUG
            int output_mutiple = sizeof(gr_complex) * d_Nb_ch;
            std::cout << std::endl << "With output_mutiple = " << output_mutiple << " ..." << std::endl;  // DEBUG
            set_output_multiple( output_mutiple );

            // Show input values of parameters
            std::cout << "d_Nb_ch: " << d_Nb_ch << std::endl;  // DEBUG
            std::cout << "d_Length_sum: " << d_Length_sum << std::endl;  // DEBUG
            std::cout << "d_Threshold: " << d_Threshold << std::endl;  // DEBUG

            // Init variables
            for(int channel = 0; channel < d_Nb_ch; channel++) {
                d_sum[channel]                 = 0.0 + 0.0j;
                d_multiplier[channel]          = 0.0 + 0.0j;
                d_state_ch[channel]            = 0;
            }
        }

        /*
        * Our virtual destructor.
        */
        Renormalize_ack_v3_impl::~Renormalize_ack_v3_impl()
        {
        }

        int Renormalize_ack_v3_impl::work(
            int noutput_items,
            gr_vector_const_void_star &input_items,
            gr_vector_void_star &output_items
        ) {
            // input, output
            const gr_complex *in = (const gr_complex *) input_items[0];
            gr_complex *out = (gr_complex *) output_items[0];
            // working variable for cleaner code
            float mean_of_d_sum = 0.0;
            float real_part_after_multiplier = 0.0;
            float imag_part_after_multiplier = 0.0;
            int   channel = 0;

            // Here we manage the counter. It counts the number of element in the sum.
            // when it arrives at the maximum value, we make a decision
            for (int i = 0; i < noutput_items; i++) {
                // increment the counter
                if (d_count < d_Length_sum) {
                    d_count++;
                    // Add a new element in the sums
                    for (channel = 0; channel < d_Nb_ch; channel++) {
                        d_sum[channel] += in[i*d_Nb_ch+channel];
                    }
                } else {
                    // add the last element of the sum
                    for (channel = 0; channel < d_Nb_ch; channel++) {
                        d_sum[channel] += in[i*d_Nb_ch+channel];
                    }

                    // Make decision as a function of the sum and as a function of the state of the channels
                    for(channel = 0; channel < d_Nb_ch; channel++) {
                        mean_of_d_sum = std::norm(d_sum[channel]) / d_Length_sum;

                        // We make decisions depending on the state of the channels
                        if (
                            (d_state_ch[channel] == 0)
                            &&
                            (mean_of_d_sum > d_Threshold)  // mean > threshold?
                        ) {
                            // The state of the channel is changed to 1 (preamble detected)
                            d_state_ch[channel] = 1;
                        }
                        else if (
                            (d_state_ch[channel] == 1)
                            &&
                            (mean_of_d_sum > d_Threshold)  // mean > threshold?
                        ) {
                            // The state of the channel is switched to 2 : in a packet
                            d_state_ch[channel] = 2;
                            // We compute here the rotation angle to map the preamble to 1+1j
                            d_multiplier[channel] = std::conj(d_sum[channel]) / std::norm(d_sum[channel]);
                            d_multiplier[channel] *= complex_unitary_symbol_00;
                        }
                        else if (
                            (d_state_ch[channel] > 0)
                            &&
                            (mean_of_d_sum <= d_Threshold)
                        ) {
                            // The state of the channel is switched to 0 : empty channel
                            d_state_ch[channel]   = 0;
                            d_multiplier[channel] = 0.0 + 0.0j;
                        }
                    }

                    // The sum and the counter are reset
                    d_count = 0;
                    for (channel = 0; channel < d_Nb_ch; channel++) {
                        d_sum[channel] = 0.0 + 0.0j;
                    }
                }

                // We manage here the output as a function of the state of the channels
                for (channel = 0; channel < d_Nb_ch; channel++) {
                    if (
                        (d_state_ch[channel] == 2)
                        &&
                        (std::norm(in[i*d_Nb_ch+channel]) > d_Threshold)
                    ) {
                        real_part_after_multiplier = std::real(in[i*d_Nb_ch+channel] * d_multiplier[channel]);
                        imag_part_after_multiplier = std::imag(in[i*d_Nb_ch+channel] * d_multiplier[channel]);
                        // The value of the output is computed depending on the value of the input multiplied by the multiplier
                        if (
                            real_part_after_multiplier >= 0 && imag_part_after_multiplier >= 0
                        ) {
                            out[i*d_Nb_ch+channel] = complex_unitary_symbol_00;
                        }
                        else if (
                            real_part_after_multiplier >= 0 && imag_part_after_multiplier <= 0
                        ) {
                            out[i*d_Nb_ch+channel] = complex_unitary_symbol_10;
                        }
                        else if (
                            real_part_after_multiplier <= 0 && imag_part_after_multiplier >= 0
                        ) {
                            out[i*d_Nb_ch+channel] = complex_unitary_symbol_01;
                        }
                        else if (
                            real_part_after_multiplier <= 0 && imag_part_after_multiplier <= 0
                        ) {
                            out[i*d_Nb_ch+channel] = complex_unitary_symbol_11;
                        } else {  // this case cannot happen, but just in case...
                            out[i*d_Nb_ch+channel] = 0.0+0.0j;
                        }
                    } else {
                        out[i*d_Nb_ch+channel] = 0.0+0.0j;
                    }
                }
            }
            // Tell runtime system how many output items we produced.
            return noutput_items;
        }
    } /* namespace Renormalize_ack */
} /* namespace gr */

